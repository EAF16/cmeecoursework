#!/usr/bin/python

"""Used to test profiling using %run -p. Includes a_useless_function,
a_less_useless_function, and some_function"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"


#IMPORTS

#Changed range to xrange because it runs faster. 
#(Range tells it to come up with the values first but xrange does it
#on demand)
def a_useless_function(x):
	"""Do y = y + 1 (with y starting at 0) for 1e8 times and return 0"""	
	y = 0
	for i in xrange(100000000):
		y = y + i
	return 0

def a_less_useless_function(x):
	"""Do y = y + 1 (with y starting at 0) for 1e5 times and return 0"""
	y = 0
	for i in xrange(100000):
		y = y + i
	return 0

def some_function(x):
	"""Print the function input and use it to run a_useless_function
	and a_less_useless function. Will print x and return 0"""
	print x
	a_useless_function(x)
	a_less_useless_function(x)
	return 0

some_function(1000)

#Q to quit out of output
