#!/usr/bin/python

"""
Plot a snapshot of a food web graph/network.

Needs: Adjacency list of who eats whom (consumer name/id in 1st
column, resource name/id in 2nd column), and list of species
names/ids and properties such as biomass (node abundance), or average
body mass.
"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#IMPORTS
#import networkx as nx because you will have to do nx."function name"
#so you don't have to worry about accidentally calling a function
#from another package
import networkx as nx
import scipy as sc
import matplotlib.pyplot as plt
#import matplotlib.animation as ani #for animation

#adjacency is who eats who
#network of probability
def GenRdmAdjList(N=2, C=0.5):
	"""Generate random adjacency list given N nodes with connectance
	probability C"""
	
	#generate IDs to recognize different nodes
	Ids = range(N)
	ALst = []
	
	#For every node, generate a link between it and evertything else
	#with a normal distribution
	#Will make a link if its random probability is greater than connectance probability
	for i in Ids:
		if sc.random.uniform(0,1,1) < C:
			Lnk = sc.random.choice(Ids, 2).tolist()
			if Lnk[0] !=Lnk[1]: #avoid self loops, no density dependence
				ALst.append(Lnk)
	return ALst

##Assign body mass range
SizRan = ([-10,10])

##Assign number of species (MaxN) and connectance (C)
MaxN = 30 #pre-allocate. network will always be 30
C = 0.75 #change from the default(0.5) to 0.75

##Generate adjacency list:
AdjL = sc.array(GenRdmAdjList(MaxN,C)) #list of connections

##Generate species (node) data:
Sps = sc.unique(AdjL) #get species ids
Sizs = sc.random.uniform(SizRan[0],SizRan[1],MaxN) #generate body sizes (log10 scale)


################
# The Plotting #
################
plt.close('all')

##Plot using networkx:
#Calculate coordinates for circular configuration:
#(see networkx.layout for inbuilt functions to compute other types of node coords)

pos = nx.circular_layout(Sps)

G = nx.Graph()
G.add_nodes_from(Sps)
G.add_edges_from(tuple(AdjL))
NodSizs= 10**-32 + (Sizs-min(Sizs))/(max(Sizs)-min(Sizs)) #node sizes in proportion to body sizes
nx.draw(G, pos, node_size = NodSizs*1000)
plt.show()
