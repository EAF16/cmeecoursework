#!/usr/bin/python

"""Working with databases in SQLite. Shows how to create the table, add
in values, and search. Prints the first two additions, how they look in
the database, and then prints all the lines one at a time"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

##IMPORTS
import sqlite3
import pdb

#pdb.set_trace()

#Connection to the database created
conn = sqlite3.connect('../Data/test.db')

# to execute commands, create a "cursor"
c = conn.cursor()

# use the cursor to execute the queries
# use the triple single quote to write
# queries on several lines
c.execute('''CREATE TABLE Test
	(ID INTEGER PRIMARY KEY,
	MyVal1 INTEGER,
	MyVal2 TEXT)''')

#c.execute('''DROP TABLE test''')

# insert the records. note that because
# we set the primary key, it will auto-increment
# therefore, set it to NULL (don't want to overwrite primary key)
c.execute('''INSERT INTO Test VALUES
	(NULL, 3, 'mickey')''')
c.execute('''INSERT INTO Test VALUES
	(NULL, 4, 'mouse')''')

# when you "commit", all the commands will
# be executed
conn.commit()

# now we select the records
c.execute("SELECT * FROM TEST")

# access the next record:
# shows the first record then the next
print c.fetchone()
print c.fetchone()

# let's get all the records at once
c.execute("SELECT * FROM TEST")
print c.fetchall()

# insert many records at once:
# create a list of tuples
manyrecs = [(5, 'goofy'),
	(6, 'donald'),
	(7, 'duck')]

#Now call executemany
#Will insert the tuples into the ? spaces
c.executemany('''INSERT INTO test
	VALUES(NULL, ?, ?)''', manyrecs)

#and commit to compile
conn.commit()

# now let's fetch the records
# we can use the query as an iterator!
for row in c.execute('SELECT * FROM test'):
	print 'Val', row[1], 'Name', row[2]

#close the connection and cut the cord before you leave
conn.close()

##################################################
# CREATE AND DISCARD A DATABASE IN YOUR WORKFLOW #
##################################################

