#!/usr/bin/python

"""Testing regular expressions, re, and finding or matching patterns.
	use of metacharacters. print match.group() and re.search()"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports
import re

#constants

#functions

##Part 1
#find a space in the string
#the r in the parentheses says to read the string in its raw form then apply
#the finds
my_string = 'a given string'
match = re.search(r'\s', my_string)

# this should print something like
#<_sre.SRE_Match object at 0x93ecdd30>
#gives position of match
print match

#now we can see what has matched
match.group()

#this should return "string" because s without backslash means literally 
#search for s followed by any character
match = re.search(r's\w*', my_string)
match.group()

# NOW AN EXAMPLE OF NO MATCH:
# find a digit in the string
# this should print "None"
match = re.search(r'\d', my_string)
print match

#Further Example
#
my_string = "an example"
match = re.search(r'\w*\s', my_string)

if match:
	print "found a match:", match.group()
else:
	print "did not find a match"


##Part 2
MyStr = "an example"

#match alphanumeric characters until you reach a space
match = re.search(r'\w*\s', MyStr)

#if there is something in match, print "found a match" and what it was.
if match:
	print "found a match:", match.group()
else:
	print "did not find a match"

##Part 3
#Some Basic Examples

#Match a digit
match = re.search(r'\d' , "it takes 2 to tango")
print match.group() #print 2

#Match a word with a space before and after
match = re.search(r'\s\w*\s' , 'once upon a time')
match.group() # 'upon'

#match a word with a space on either side and between 1 and 3 characters
match = re.search(r'\s\w{1,3}\s' , 'once upon a time')
match.group() # ' a '

#match a word with a space before but not after
match = re.search(r'\s\w*$' , 'once upon a time')
match.group() # ' time'

#match a word with no space preceeding until a digit then do again
#must end with a number
match = re.search(r'\w*\s\d.*\d' , 'take 2 grams of H2O')
match.group() # 'take 2 grams of H2'

#start at the beginning of the line and match any letters and any 
#characters (including a space) then stop at the last character followed by a space
match = re.search(r'^\w*.*\s' , 'once upon a time')
match.group() # 'once upon a'

## NOTE THAT *. +, and { } are all "greedy":
## They repeat the previous regex token as many times as possible
## as a result, they may match more text than you want 

## To make it non-greedy, use ?:
#Starts at beginning of the line, matches letters including followed by a space
match = re.search(r'^\w*.*?\s' , 'once upon a time')
match.group() # 'once '

## To further illustrate greediness, let's try matching an HTML tag:
match = re.search(r'<.+>', 'This is a <EM>first</EM> test')
match.group() # '<EM>first</EM>'
## But we didn't want this: we wanted just <EM>
## It's because + is greedy!

## Instead, we can make + "lazy"!
match = re.search(r'<.+?>', 'This is a <EM>first</EM> test')
match.group() # '<EM>'

## OK, moving on from greed and laziness
#Finds any digit then a . then finds any digit until new characters come up
match = re.search(r'\d*\.?\d*','1432.75+60.22i') #note "\" before "."
match.group() # '1432.75'

#Doesn't return the same as the last one because it's looking for a period
#not any character like a +
match = re.search(r'\d*\.?\d*','1432+60.22i')
match.group() # '1432'

#Match A then if not, match G, then if not, match T, if not match C
#greedy so continuous, without the plus, it finds one match then stops
match = re.search(r'[AGTC]+', 'the sequence ATTCGT')
match.group() # 'ATTCGT'

#Matches a space once followed by any uppercase letter once then any other
#letters til you hit a space then the whole next word
#+ forces it to find the next match
re.search(r'\s+[A-Z]{1}\w+\s\w+', 'The bird-shit frog"s name is Theloderma asper').group() #' Theloderma asper'

## NOTE THAT I DIRECTLY RETURNED THE RESULT BY APPENDING .group()
