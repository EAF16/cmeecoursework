#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: run_LV.sh
# Desc: Runs the set of LV scripts and profiles them
# Arguments: none
# Date: 26 NOV 2016

#Run the scripts 
echo -e "\nRunning LV1.py"
python LV1.py

echo -e "\nRunning LV2.py with INPUTS\n"
python LV2.py 1. 0.1 1.5 0.75 30

echo -e "\nRunning LV3.py with DEFAULTS\n"
python LV3.py

echo -e "\nRunning LV4.py with DEFAULTS\n"
python LV4.py

echo -e "\nRunning LV5.py with DEFAULTS\n"
python LV5.py

echo -e "\nRunning LV6.py with DEFAULTS\n"
python LV6.py


#Profiling the results
echo -e "\nRunning the PROFILER on LV1.py"
python -m cProfile LV1.py

echo -e "\nRunning the PROFILER on LV2.py"
python -m cProfile LV2.py 1. 0.1 1.5 0.75 30

echo -e "\nRunning the PROFILER on LV3.py"
python -m cProfile LV3.py

echo -e "\nRunning the PROFILER on LV4.py"
python -m cProfile LV4.py

echo -e "\nRunning the PROFILER on LV5.py"
python -m cProfile LV5.py

echo -e "\nRunning the PROFILER on LV6.py"
python -m cProfile LV6.py
