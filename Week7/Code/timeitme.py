#!/usr/bin/python

"""This script is used to practice profiling using a number of different
functions to test the difference in how long they take to run using
timeit"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"


#IMPORTS
import time #get timeit function
import timeit

#Get all functions at once
#from timeitme import *

####################
# range vs. xrange #
####################

def a_not_useful_function():
	"""Do y = y + 1 (with y starting at 0) for 1e5 times and return 0
	with range"""
	y = 0
	for i in range(100000):
		y = y + 1
	return 0

def a_less_useless_function():
	"""Do y = y + 1 (with y starting at 0) for 1e5 times and return 0
	with xrange"""
	y = 0
	for i in xrange(100000):
		y = y + 1
	return 0

#One approach for timing
#say start is the current clock time
start = time.time()
a_not_useful_function()
print "a_not_useful_function takes %f s to run." % (time.time() - start)

start = time.time()
a_less_useless_function()
print "a_less_useless_function takes %f s to run." % (time.time() - start)

#But running it will give a different time for each run so you can also
#do this in python:
#RUN THESE USING %PASTE
#%timeit a_not_useful_function()
#%timeit a_less_useless_function()

#xrange is faster

#####################################
# for loops vs. list comprehensions #
#####################################

my_list = range(1000)

def my_squares_loop(x):
	"""Loop method of finding square i in range x and add to list"""
	out = []
	for i in x:
		out.append(i ** 2)
	return out

def my_squares_lc(x):
	"""List comprehension method of finding square i in range x and add 
	to list"""
	out = [i ** 2 for i in x]
	return out
	
#%timeit(my_squares_loop(my_list))
#%timeit(my_squares_lc(my_list))

#The lc method is faster

#############################
# for loops vs. join method #
#############################
import string
my_letters = list(string.ascii_lowercase)

def my_join_loop(l):
	"""Loop method for adding letters from l to out"""
	out = ''
	for letter in l:
		out += letter
	return out

def my_join_method(l):
	"""Join method for adding letters from l to out"""
	out = ''.join(l)
	return out

#%timeit(my_join_loop(my_letters))
#%timeit(my_join_method(my_letters))

#Join method is faster

###########
# Oh dear #
###########

def getting_silly_pi():
	"""Starting at 0, y is y + 1 up to 100000"""
	y = 0
	for i in xrange(100000):
		y = y + 1
	return 0

def getting_silly_pii():
	"""Starting at 0, y is y+=1 up to 100000"""
	y = 0
	for i in xrange(100000):
		y += 1
	return 0

#%timeit(getting_silly_pi())
#%timeit(getting_silly_pii())
	
#They basically tell it to do the same thing and one doesnt seem 
#particualrly faster than the other
