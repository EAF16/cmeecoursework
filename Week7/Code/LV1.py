#!/usr/bin/python

""" The typical differential Lotka-Volterra Model simulated using scipy 
"""

__author__ = "Samraat Pawar (annotated by Emma Fox)"
__version__ = "0.0.2"

#IMPORTS
#Import these functions and use shorter commands to refer to them
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import cProfile
import pstats

# import matplotlip.pylab as p #Some people might need to do this

#Use pops at t=0, gives beginning step so you dont have to specify later
def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    #R is resources at the beginning of the pops set
    #Consumers are the second term in the pops set
    R = pops[0]
    C = pops[1]
    #Equations
    dRdt = r*R - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    #Return array showing changes for singular time step
    return sc.array([dRdt, dCdt])

# Define parameters:
r = 1. # Resource growth rate
a = 0.1 # Consumer search rate (determines consumption rate) 
z = 1.5 # Consumer mortality rate
e = 0.75 # Consumer production efficiency
#The units these are in will determine the scale of t (days, years, etc)

# Now define time -- integrate from 0 to 15, using 1000 points/slices:
t = sc.linspace(0, 15,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area
#function takes both so output array will have both

#Could run 1000s of iterations of functions or integrate will run it
#with the specified number of slices
#odeint is integrate ordinary differential equations
pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

#Formats figure
#Pairs prey and predator name with column places after dictionary output
#is transposed
prey, predators = pops.T # What's this for?
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics')
#show the graph
#p.show()
#save
f1.savefig('../Results/prey_and_predators_1.pdf') #Save figure
