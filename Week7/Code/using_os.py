#!/usr/bin/python

"""This script finds all files and directories beginning with uppercase
C, all files and directories beginning with c, and all directories 
beginning with c and puts them all in respective lists"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

# Use the subprocess.os module to get a list of files and directories 
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

##IMPORTS
import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []

# Use a for loop to walk through the home directory.
#Gets the dirnames and filenames that walk prints out as its 2nd and 3rd 
#terms and appends them to the list if they start with C
for (dir, subdir, files) in subprocess.os.walk(home):
	for dirnames in subdir:
		if dirnames.startswith("C"):
			FilesDirsStartingWithC.append(dirnames)
	for filenames in files:
		if filenames.startswith("C"):
			FilesDirsStartingWithC.append(filenames)
#print FilesDirsStartingWithC

x=set(FilesDirsStartingWithC)
print "\nThe number of directories and files starting with the uppercase C is %r" % len(x)
print "Enter 'FilesDirsStartingWithC' to view matches"

#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'


# Create a list to store the results.
FilesDirsStartingWithCorc = []

# Use a for loop to walk through the home directory.
#Gets the dirnames and filenames that walk prints out as its 2nd and 3rd 
#terms and appends them to the list if they start with C or c
for (dir, subdir, files) in subprocess.os.walk(home):
	for dirnames in subdir:
		if dirnames.startswith("C") or dirnames.startswith("c"):
			FilesDirsStartingWithCorc.append(dirnames)
	for filenames in files:
		if filenames.startswith("C") or filenames.startswith("c"):
			FilesDirsStartingWithCorc.append(filenames)

#Print results
#print FilesDirsStartingWithCorc

#Print number of results
y=set(FilesDirsStartingWithCorc)
print "\nThe number of directories and files starting with the letter C is \
%r" % len(y)
print "Enter 'FilesDirsStartingWithCorc' to view matches"

#################################
# Get only directories in your home/ that start with either an upper or 
# lower case 'C' 

# Create a list to store the results.
DirsStartingWithCorc = []

# Use a for loop to walk through the home directory.
#Gets the dirnames that walk prints out as its 3rd terms and appends 
#them to the list if they start with C or c
for (dir, subdir, files) in subprocess.os.walk(home):
	for dirnames in subdir:
		if dirnames.startswith("C") or dirnames.startswith("c"):
			DirsStartingWithCorc.append(dirnames)

#Print results
#print DirsStartingWithCorc

#Print number of results
z=set(DirsStartingWithCorc)
print "\nThe number of directories starting with the letter C is %r" % len(z)
print "Enter 'DirsStartingWithCorc' to view matches"
