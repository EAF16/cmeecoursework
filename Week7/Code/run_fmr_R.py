#!/usr/bin/python

"""Run fmr.R in python, say whether it has been successful, and list the 
R console output"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

##IMPORTS
import subprocess
import os.path

#Runs the fmr.R script
subprocess.Popen("Rscript --verbose fmr.R", shell=True).wait()


#Checks to see if the graph was created
print "\n\nChecking to see if the graph was created...\n"

if os.path.isfile("../Results/fmr_plot.pdf"):
	print "The graph has been created SUCCESSFULLY"
else:
	print "The graph does not exist"
