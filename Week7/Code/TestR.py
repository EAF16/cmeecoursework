#!/usr/bin/python

"""Running R scripts using subprocess and saving the console and error
outputs"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#IMPORTS
import subprocess

#Could also just run Rscript directly because it is in the bin
#(find /usr -name 'Rscript')
#verbose tells it to write the output
#Output file in the R output
#err_file is what is actually bein run (the log)
subprocess.Popen("/usr/lib/R/bin/Rscript --verbose TestR.R > \
../Results/TestR.Rout 2> ../Results/TestR_errFile.Rout",\
shell=True).wait()


#Type directly into ipython
#~ subprocess.Popen("/usr/lib/R/bin/Rscript --verbose NonExistScript.R > \
#~ ../Results/outputFile.Rout 2> ../Results/errorFile.Rout", \
#~ ...: shell=True).wait()
