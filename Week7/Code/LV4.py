#!/usr/bin/python

"""Running density dependent lotka volterra DISCRETE TIME models with 
commandline inputs for r, a, z, e. Outputs a graph and the final 
population sizes. Includes defaults to produce a population in 
equilibrium with prey density dependence"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#IMPORTS
#Import these functions and use shorter commands to refer to them
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys
import cProfile
import pstats
import numpy as np

#Starts the array with the initial population values
pops = sc.array([[0,10,5]])

# Define parameters from commandline:
#and include defaults to produce the equilibrium graph

if len(sys.argv) >1:
	r = float(sys.argv[1]) # Resource growth rate
	a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
	z = float(sys.argv[3]) # Consumer mortality rate
	e = float(sys.argv[4]) # Consumer production efficiency
	K = float(sys.argv[5]) #Carrying capacity, 30 is a good value
elif len(sys.argv)==1:
	r = 1. #2
	a = 0.1 #.99
	z = 1.5
	e = 0.75 
	K = 30. #27
	
#A for loop that makes the equations for R and C reference the R and C
#values in the year before them/in the row above them in the array
for i in range(1,16):
	x=pops[i-1,1]
	y=pops[i-1,2]
	R=x*(1+r*(1-(x/K))-a*y)
	C=y*(1-z+e*a*x)
	pops = np.r_[pops, [(i,R,C)]]

#Prints the array of the population sizes in different years	
#~ print pops


#Formats figure
#Pairs time, prey, and predator name with column places after dictionary 
#output is transposed
t, prey, predators = pops.T # What's this for?
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics \n(r=%r, a=%r, z=%r, e=%r, K=%r)' % (r,a,z,e,K))
f1.savefig('../Results/prey_and_predators_4.pdf') #Save figure

#Print final population sizes
print "When both populations are pesisting under these conditions with prey density dependence, the final population sizes are..."
print "\n %d producers and %d consumers" % (pops[-1,0],pops[-1,1])
print "\n (r=%r, a=%r, z=%r, e=%r, K=%r)" % (r, a, z, e, K)

