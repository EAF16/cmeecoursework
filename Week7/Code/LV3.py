#!/usr/bin/python

"""Running density dependent lotka volterra models with commandline 
inputs for r, a, z, e. Outputs a graph and the final population sizes.
Includes defaults to produce a population in equilibrium with prey
density dependence"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#IMPORTS
#Import these functions and use shorter commands to refer to them
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
import sys
import cProfile
import pstats

# import matplotlip.pylab as p #Some people might need to do this

#Use pops at t=0, gives beginning step so you dont have to specify later
def dCR_dt(pops, t=0):
    """ Returns the growth rate of predator and prey populations at any 
    given time step """
    
    #R is resources at the beginning of the pops set
    #Consumers are the second term in the pops set
    R = pops[0]
    C = pops[1]
    #Equations
    dRdt = r*R*(1-(R/K)) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    #Return array showing changes for singular time step
    return sc.array([dRdt, dCdt])

# Define parameters from commandline:
#and include defaults to produce the equilibrium graph

if len(sys.argv) >1:
	r = float(sys.argv[1]) # Resource growth rate
	a = float(sys.argv[2]) # Consumer search rate (determines consumption rate) 
	z = float(sys.argv[3]) # Consumer mortality rate
	e = float(sys.argv[4]) # Consumer production efficiency
	K = float(sys.argv[5]) #Carrying capacity, 30 is a good value
elif len(sys.argv)==1:
	r = 1. #2
	a = 0.1 #.99
	z = 1.5
	e = 0.75 
	K = 30 #27
#The units these are in will determine the scale of t (days, years, etc)

# Now define time -- integrate from 0 to 15, using 1000 points/slices:
t = sc.linspace(0, 15,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initials conditions: 10 prey and 5 predators per unit area
#function takes both so output array will have both

#Could run 1000s of iterations of functions or integrate will run it
#with the specified number of slices
#odeint is integrate ordinary differential equations
pops, infodict = integrate.odeint(dCR_dt, z0, t, full_output=True)

infodict['message']     # >>> 'Integration successful.'

#Formats figure
#Pairs prey and predator name with column places after dictionary output
#is transposed
prey, predators = pops.T # What's this for?
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics \n(r=%r, a=%r, z=%r, e=%r, K=%r)' % (r,a,z,e,K))
#~ p.show()
f1.savefig('../Results/prey_and_predators_3.pdf') #Save figure


#Extra Credit: Prints the final populations sizes for a stable population
print "When both populations are pesisting under these conditions with prey density dependence, the final population sizes are..."
print "\n %d producers and %d consumers" % (pops[-1,0],pops[-1,1])
print "\n (r=%r, a=%r, z=%r, e=%r, K=%r)" % (r, a, z, e, K)
