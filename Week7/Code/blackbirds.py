#!/usr/bin/python

"""Using regular expressions (including r.findall and r.compile) to 
output the kingdom, phylum, and species from a text file"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"


#Imports
import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = str(text.decode('ascii', 'ignore'))

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

# my_reg = ??????

# Hint: you will probably want to use re.findall(my_reg, text)...
# Keep in mind that there are multiple ways to skin this cat! 

#re.findall finds all matches 
#?<=\b removes the word we type in next so that we get the following one
#So we find kingdom then look for a space and take the next string of letters
#Same thing for phylum then finds the next two strings after species
my_reg = re.findall(r'((?<=\bKingdom\s)[\w]*).*?((?<=\bPhylum\s)[\w]*).*?((?<=\bSpecies\s)[\w]*\s[\w]*)', text)
my_reg

#For each set in my_reg, print the first term as the kingdom, the second,
#as the phylum, and the third as the species
for i in my_reg:
	print "\nThis blackbird belongs to..."
	print "Kingdom:",i[0]
	print "Phylum",i[1]
	print "Species:",i[2]
	
	
