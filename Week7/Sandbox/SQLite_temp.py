#!/usr/bin/python

"""This script pracitces creating and discarding a SQLite Database in 
your workflow. To be run line by line."""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1" 

##IMPORTS
import sqlite3

#Create a connection to the temporary database
conn = sqlite3.connect(":memory:")

c = conn.cursor()

c.execute("CREATE TABLE tt (Val TEXT)")

#commit and compile
conn.commit()

#Create values
z = [('a',), ('ab',), ('abc',), ('b',), ('c',)]

#Add Z in 
c.executemany("INSERT INTO tt VALUES (?)", z)

#commit and compile
conn.commit()

#Look at some values
c.execute("SELECT * FROM tt WHERE Val LIKE 'a%'").fetchall()

#Cut the cord
conn.close()
