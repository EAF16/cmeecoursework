#FileName: EAF16.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

rm(list = ls())
graphics.off()

#Tells iter to be the number of the CPU running the code
iter<-as.numeric(Sys.getenv("PBS_ARRAY_INDEX"))

#Making 4 different population size runs
if (iter %%4==0){
  size<-500}
if (iter %%4==1){
  size<-1000}
if (iter %%4==2){
  size<-2500}
if (iter %%4==3){
  size<-5000}

#Setting parameters
speciation_rate<-0.005436
wall_time<-41400
rand_seed<-iter
interval_rich<-size/2
interval_oct<-size/10
burn_in_time<-size*8

##################################
# NECESSARY SUPPORTING FUNCTIONS #
##################################
#1.)Counts the number of unique species
species_richness<-function(CommVec){
  richness<-length(unique(CommVec))
  return(richness)
}

#2.)Initial state with max number of possible species (basically number of species = vector length)
initialise_max<-function(x){
  NewMaxCommVec<-c(seq(from = 1, to = x, by = 1))
  return(NewMaxCommVec)
}

#4.)Chooses a random number from a normal distribution between 1 and x. Choose another random number not
#   equal to the first then return both as a vector
choose_two<-function(x){
  return(sample.int(x, size=2, replace = FALSE))
}

#8.)A new equation to replace a dead species with a newly formed species rather than an existing species
#   with a probability of v
neutral_step_speciation<-function(community,v){
  spot<-choose_two(length(community))  
  if ((runif(1, 0, 1))>v){
    community[spot[1]]<-community[spot[2]]
  }
  else{
    community[spot[1]]<-max(community)+1
  }
  return(community)  
}

#9.)Run the simulation for the specified number of steps with speciation accounted for
#Return in a list: 
#   a time series of species richness for every interval during the duration specified 
#   a vector of the final species identities
neutral_time_series_speciation<-function(initial,duration,interval,v){
  community <- initial
  RichSeries<-c(species_richness(community))  
  for (i in 1:duration){
    community<-neutral_step_speciation(community, v)
    if (i %% interval ==0){
      richness<-species_richness(community)
      RichSeries=c(RichSeries, richness)
    }
  }
  results=list(RichSeries, community)  
  return(results)
}

#11.)Counts the abundance of each species and lists them from most abundant to least
species_abundance<-function(community){
  x<-sort(table(community), decreasing = TRUE)
  return(as.vector(x))
}

#12.)Puts species abunance in octave classes defined by 2^(n-1) and 2^n
#    Use floor to round down to the nearest ineger after logging
#    Add 1 so that way tabulate doesn't delete bin 0
#    Tabulate puts the same integers resulting from the log together
octaves<-function(abundance){
  bins<-tabulate(floor(log2(abundance)+1))
  return(bins)
}

###############
# CLUSTER RUN #
###############

#Runs the function on the HPC cluster
cluster_run<-function(speciation_rate, size, wall_time, rand_seed, interval_rich, interval_oct, burn_in_time){
  #sets the length of a generation
  gen_time<-size/2

  #sets the time limit as the current time plus wall_tie
  start<-as.numeric(proc.time()[3])
  finish<-start+wall_time
  
  #Create a new community and set that as generation 0
  community <- initialise_max(size)
  gen_count<-0
  
  #Create a new vector for species richness and a new list for octaves of generation 0
  RichSeries<-(species_richness(community))
  OctaveSeries<-octaves(species_abundance(community)) #list
  
  #While there is still time left
  while (as.numeric(proc.time()[3])<finish){
    #Run the simulation for enough years to make a generation
    for (i in gen_time){
      community<-neutral_step_speciation(community, speciation_rate)
    }
    #If the generation count is less than the burn in time and divisble by interval_rich, record species richness
    if ((gen_count<=burn_in_time)&&(gen_time %% interval_rich ==0)){
      RichSeries<-c(RichSeries, species_richness(community))
    }
    #If the generation count is divisble by the interval_oct, record the octave
    if (gen_count %% interval_oct ==0){
      OctaveSeries<-c(OctaveSeries, list(octaves(species_abundance(community))))
    }
    #move the generation count up by 1
    gen_count<-gen_count + 1
  }
  #Returns the total run time
  runtime<-proc.time()[3]-start
  #Saves the richness vector, octave list, final community, run time, and parameters
  #in a file named according to the random seed. 
  save(RichSeries, OctaveSeries, community, runtime, speciation_rate, size, wall_time, rand_seed, interval_rich, interval_oct, burn_in_time, 
       file=paste0("Output_rand_seed_",rand_seed,".Rda"))
}

#Runs the function
cluster_run(speciation_rate, size, wall_time, rand_seed, interval_rich, interval_oct, burn_in_time)