#Chaos Game
chaos_game(5000)

#Testing Turtle
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(0, 4), ylim=c(0, 4))
q<-c(1,1)
turtle(q, pi, 2)

#Testing Elbow
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(-4, 4), ylim=c(-4, 4))
q<-c(1,1)
elbow(q, pi, 2)

# #Testing spiral
# plot.new()
# plot(1, type="n", xlab="", ylab="", xlim=c(0, 20), ylim=c(0, 20))
# q<-c(10,10)
# spiral(q, pi, 5)

#Testing spiral_2
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(0, 15), ylim=c(0, 20), axes=FALSE)
q<-c(10,5)
spiral_2(q, pi, 5)

#Testing tree
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(0, 20), ylim=c(0, 15), axes=FALSE)
q<-c(10,0)
tree(q, pi/2, 5)

#Testing fern
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(0,8), ylim=c(0, 8), axes=FALSE)
q<-c(2,0)
fern(q, pi/2, 1)

#Testing fern_2
#plot.new()
plot(1, type="n", xlab="", ylab="", xlim=c(0,8), ylim=c(0, 8), axes=FALSE)
q<-c(3,0)
fern_2(q, pi/2, 1)
