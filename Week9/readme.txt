readme for Week9 Directory


├── Code
│   ├── EAF16_HPC.R - Code to run the simulation on the cluster
│   ├── EAF16.R - All of the code needed for the handout
│   └── Handout_Answers.tex - The LaTeX code for the write-up
├── Data
│   ├── Chaos_Game_5000.pdf - fractal chaos game figure for write-up
│   ├── Fern_2.pdf - fractal fern_2 figure for write-up
│   ├── Fern.pdf - fractal fern figure for write-up
│   ├── Question_10.pdf - fractal question 10 figure for write-up
│   ├── Question_14.pdf - fractal question 14 figure for write-up
│   ├── Question_17.pdf - fractal questions 17 figure for write-up
│   ├── Question_7.pdf - fractal question 7 figure for write-up
│   ├── Spiral_2.pdf - fractal spiral 2 figure for write-up
│   └── Tree.pdf - fractal tree figure for write-up
├── Results
│   ├── Handout_Answers.pdf - compiled LaTeX document of hand out answers
│   └── HPC_Run_Output.tgz - zipfile output from the cluster run. Includes R code to be run, bash shell script instructions, .RDA results files, and command line and error files (.o and .e)
└── SandBox
    ├── Fractals.R - draft of fractal work included in EAF16.R
    ├── Testing.R - test lines for running the functions in Fractals.R
    ├── TestingTimer.R - used to experiment with timing system in r
    └── TestRun
        └── Output_rand_seed_.tgz - ran a scaled down test run on the cluster. zipfile conatins all outputs

