readme for Week2 Directory

***************
DATA FOLDER:
***************
TestOaksData.csv - List of tree species for test_oaks.py to be run on.

TestSeqs.csv - File containing the sequences used for the align_seqs.py 
				script. Part of practical set 2.
				
*Fasta Folder
07228412.fasta - Contains a Homo sapiens tryptase beta 2 gene sequence. 
					Used to test practical set 2 code align_seq_fasta.
407228326.fasta - Contains a Homo sapiens x Mus musculus hybrid cell line 
					gene sequence. Used to test practical set 2 code align_seq_fasta.
E.coli.fasta - Contains an E. coli gene sequence. 

***************
CODE FOLDER:
***************
basic_io.py - Prints test.txt (from ../Sandbox) then prints test.txt 
				without spaces between lines. Saves a list of 100 
				integers starting from 0 in as testout.txt in Sandbox.  
				Then creates, prints, and saves a dictionary with "a 
				key" and "another key" as testp.p in Sandbox.

basic_csv.py - Reads testcsv.csv and lists species by name then creates 
				a document listing species and their weight.

boilerplate.py - Prints the phrase "This is a boilerplate"

using_name.py - Prints either "This program is being run by itself" or 
				"I am being imported from another module depending on 
				whether it is simply being run or imported. Used to 
				exemplify the __name__ variable

sysargv.py - Used to explore the argv argument variable. Lists the name 
				of the script, number of the arguments, and what the
				names of the argument are. 

scope.py - Used for exploring the concept of global variables within and
			outside a function.

control_flow.py - Codes for 4 functions: even_or_odd, largest_divisor_five, 
					is_prime, find_all_primes. Can input a number and will
					output whether the number is odd, it's largest divisor 
					up to 5, whether the number is prime, and how many prime
					numbers there are between 2 and the input. 

cfexercises.py - Codes for using control statements to print a series of
					hellos and define a series of foo's to perform numerical
					operations.

loops.py - Practice for creating loops. Use of while. Will need to use 
			ctrl+c to exit. 

oaks.py - Shows how to construct a set of matches from a list both with
			loops and list comprehension. 

test_control_flow.py - Used the even_or_odd function to practice testing units 
						using doctest.

debugme.py - Created a function with an impossible division to learn how to do
				debugging with %pdb.

1c1.py - Part of practical set 2. Code for seperating bird species tuples
			into lists of latin names, common names, and mean body mass.

1c2.py - Part of practical set 2. Code for making lists out of tuple terms 
			regarding monthly rainfall that fulfill certain arguments 
			(ie greater or less than a certain value).
			
dictionary.py - Part of practical set 2. Code for putting a tuple into a 
				dictionary nested in sets by taxa.

tuple.py - Part of practical set 2. Code for printing a tuple on seperate 
			lines. 

test_oaks.py - Part of practical set 2. Exercise in debugging a file. Code
				is for finding oak species from TestOaksData.csv and 
				printing the list in JustOaksData.csv

align_seqs.py - Code for function finding the best alignment of two sequences. 
				Looks for the greatest number of matches between a sqeuence 
				and another shifted along it. Part of practical 2.

align_seqs_fasta.py - Code for finding the best alignment of two sequences 
					stored in two different fasta files. Looks for the 
					greatest number of matches between a sequence and 
					another shifted along it. Part of practical 2.

***************
RESULTS FOLDER
***************
JustOaksData.csv - Resultant list of oak species from running test_oaks.py
					on TestOaksData.csv
					
AlignedTestSeqs.txt - File containing the best alignment and score for
					the test sequences given in align_seqs.py. Part of 
					practical 2. 

407228326_407228412.txt - File containing the best alignment and score for
					the test fasta sequences. Part of practical 2. 

***************
SANDBOX FOLDER
***************
test.txt - Includes text used to test basic_io.py (list including First
			Line, Second Line, etc.)

testout.txt - Created by basic_io.py and contains a list of 100 integers
				from 0 to 99.

test.p - Created by basic_io.py Stores dictionary including "a key" 
			(equal to 10) and "another "key" (equal to 11).


testcsv.csv - A list of species including their infraorder, family, 
				distribution, body mass male (Kg). Used for basic_csv.py

bodymass.csv - Created by basic_csv.py. Lists species from testcsv.csv 
				with their body mass. 

