#!/usr/bin/python

"""Lists the name of the script, number of the arguments, and what the
names of the argument are"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

import sys
print "This is the name of the script: ", sys.argv[0]
print "Number of arguments: ", len (sys.argv)
print "The arguments are: ", str(sys.argv)

# agrv holds the space for what you input 
