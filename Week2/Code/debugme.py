#!/usr/bin/python

"""Creates a bug in the form of an division by zero to practice debugging with 
%pdb and ipdb"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

def createabug(x): 
	"""Has an intentioal bug. Trying to divide by 0. Used to practice 
	work with ipdb"""
	y = x**4
	z = 0.
	#stops the code so it can be run step by step to detect where the 
	#error occurs
	import ipdb; ipdb.set_trace()
	#impossible to divide by 0
	y = y/z
	return y

createabug(25) 
