#!/usr/bin/python

"""Analysing a tuple of month and rainfall to create a list of months 
when the rainfall exceeded 100mm and a list for when it was below 50 mm
using both list comprehensions and conventional loops. Part of practical
2."""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports

#constants

#functions

# Average UK Rainfall (mm) for 1910 by month
# http://www.metoffice.gov.uk/climate/uk/datasets
rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )

# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS



#(1) List comprehension greater than 100mm

#Create a list called high_rainfall_lc and fills it by running through
#each entry (i) in rainfall and adding the full entry (i) if
#the second part of the entry is greater than 100.0mm.
high_rainfall_lc = list (i for i in rainfall if i[1] > 100.0)
print high_rainfall_lc

#(2) List comprehension less than 50mm
#Create a list called low_rainfall_lc and fills it by running through
#each entry (i) in rainfall and adding the first part of the 
#entry (i) if the second part of the entry is less than 50.0mm.
low_rainfall_lc = list (i[0] for i in rainfall if i[1] < 50.0)
print low_rainfall_lc

#(3) Conventional loops

#Creates an empty list called high_rainfall_loops
#The function then tells python that
#for each entry in rainfall
#if the second part is greater than 100.0mm
#add the whole entry to high_rainfall_loops
#Then print the high_rainfall_loops list
high_rainfall_loops = []
for i in rainfall:
	if i[1] > 100.0:
		high_rainfall_loops.append(i)
print high_rainfall_loops

#Creates an empty list called low_rainfall_loops
#The function then tells python that
#for each entry in rainfall
#if the second part is less than 50.0mm
#add the first part of entry to low_rainfall_loops
#Then print the low_rainfall_loops list
low_rainfall_loops = []
for i in rainfall:
	if i[1] < 50.0:
		low_rainfall_loops.append(i[0])
print low_rainfall_loops
