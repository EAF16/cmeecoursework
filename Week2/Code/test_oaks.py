#!/usr/bin/python

"""For practical 2. Debugging and using doctest to fix oak finding function"""

__author__ = "Samraat Pawar, edited by Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.2"

#IMPORTS

import csv
import sys
import pdb
import doctest
import ipdb

#FUNCTION

#Define function
#Function tells whether the name from the input is an oak by evalauting
#whether the name matches quercus exaclty (after being converted to lower
#case).
#The doctest should return true for the first test because it is the 
#desired result. The second is false because it is the wrong genus. The
#third is there to make sure only results will the full word quercus are
#returned and not those with quercus as only part.
def is_an_oak(name):
    """ Returns True if name is starts with 'quercus '
        >>> is_an_oak('Quercus')
        True
        
        >>> is_an_oak('Fagus sylvatica')
        False
        
        >>> is_an_oak ('Quercuss')
        False
    """
    
    return name.lower()==('quercus')

#Prints doctest
print(is_an_oak.__doc__)

#Unless otherwise specified, the function will open the TestOaksData set
#to run istelf on and print the results in JustOaksData.
#Creates taxa from the open read file.
#Makes blank set 'oaks'
#For each row in taxa, print the whole row, print a line telling us what 
#the genus is, and then run is_an_oak on the first part of the row. 
#If it is an oak, print the genus again, say 'found an oak', and add
#it to the new set in the new .csv file
def main(argv): 
	"""Opens a file containing species names and writes a new file
	containing just the oak names"""
    f = open('../Data/TestOaksData.csv','rb')
    g = open('../Results/JustOaksData.csv','wb')
    taxa = csv.reader(f)
    csvwrite = csv.writer(g)
    oaks = set()
    for row in taxa:
        print row
        print "The genus is", row[0]
        #import ipdb; ipdb.set_trace()
        if is_an_oak(row[0]):
            print row[0]
            print 'FOUND AN OAK!'
            print " "
            csvwrite.writerow([row[0], row[1]])    
    
    return 0
    
if (__name__ == "__main__"):
    status = main(sys.argv)

doctest.testmod()
