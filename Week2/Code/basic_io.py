#!/usr/bin/python

"""Prints test.txt (from ../Sandbox) then prints test.txt without spaces 
between lines. Saves a list of 100 integers starting from 0 in as 
testout.txt in Sandbox. Then creates, prints, and saves a dictionary 
with "a key" and "another key" as testp.p in Sandbox."""

___author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"


##############################
# FILE INPUT
##############################
# Open a file for reading
f = open ("../Sandbox/test.txt", "r")

# use "implicit" for loop:
# if the object is a file, python will cyle over lines
for line in f:
	print line, # the "," prevents adding a new line

# close the file
f. close()

# same example, skip blank lines
#if the has greater than 0 characters in it, print
f = open ("../Sandbox/test.txt", "r")
for line in f:
	if len(line.strip()) > 0:
		print line,

#Escape opened file
f.close()

#############################
# FILE OUTPUT
#############################
# Save the elements of a list to a file
list_to_save = range(100)

f = open("../Sandbox/testout.txt", "w")
for i in list_to_save:
	f.write(str(i) + "\n") ## Add a new line at the end
	
f.close()	

#############################
#STORING OBJECTS
#############################
# To save an object (even complex) for later use
my_dictionary = {"a key": 10, "another key": 11}

import pickle

#dump the dictionary into the open f file.
f = open("../Sandbox/testp.p", "wb") ## note the b: accept binary failures
pickle.dump(my_dictionary, f)
f.close()

## Load the data again into the pickle
f = open("../Sandbox/testp.p", "rb")
another_dictionary = pickle.load(f)
f.close()

#Print the dictionary we saved and subsequently reloaded under another name
print another_dictionary
