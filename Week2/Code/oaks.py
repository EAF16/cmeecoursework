#!/usr/bin/python

"""Practice using for loops versus list comprehensions to make a set of
	oaks from a taxa list"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports

#constants

#functions
##Let's find just those taxa that are oak trees from a list of species

taxa = [ "Quercus robur", "Fraxinus excelsior", "Pinus sylvestris", 
		"Quercus cerris", "Quercus petraea", ]

#lower case the names and return quercus names. 

def is_an_oak(name):
	"""return any species name starting with quercus"""
	return name.lower().startswith("quercus ")

##Using for loops
#making an empty set to add the entries we want. 
#explicit loop gives a range. this is implicit (run through all strings)
#if it's in is_an_oak, put in the list. then print the whole list
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species)
print oaks_loops


##Using list comprehensions
#creates a set then fills with species names from taxa which is_an_oak returns.
oaks_lc = set([species for species in taxa if is_an_oak(species)])
print oaks_lc


##Get names in UPPER CASE using for loops
oaks_loops = set()
for species in taxa:
	if is_an_oak(species):
		oaks_loops.add(species.upper())
print oaks_loops

##Get names in upper case using list comprehensions
oaks_lc = set([species.upper() for species in taxa if is_an_oak(species)])
print oaks_lc

