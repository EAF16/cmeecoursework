#!/usr/bin/python

"""Creating a dictionary that clusters a tuple in taxa group sets. Part
of practical 2."""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports

#constants

#functions

taxa = [ ('Myotis lucifugus','Chiroptera'),
         ('Gerbillus henleyi','Rodentia',),
         ('Peromyscus crinitus', 'Rodentia'),
         ('Mus domesticus', 'Rodentia'),
         ('Cleithrionomys rutilus', 'Rodentia'),
         ('Microgale dobsoni', 'Afrosoricida'),
         ('Microgale talazaci', 'Afrosoricida'),
         ('Lyacon pictus', 'Carnivora'),
         ('Arctocephalus gazella', 'Carnivora'),
         ('Canis lupus', 'Carnivora'),
        ]

# Write a short python script to populate a dictionary called taxa_dic 
# derived from  taxa so that it maps order names to sets of taxa. 
# E.g. 'Chiroptera' : set(['Myotis lucifugus']) etc. 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS

# Write your script here:


##CAN PUT IN BELLS AND WHISTLES

#Create a new dictionary
taxa_dic = {}

#Run loop through the species, key tuples in taxa
#If the key doesn't exist,
#Make a new set from that key
#If the key does exist, add the species to that key
for (species,key) in taxa:
	if key not in taxa_dic:
		taxa_dic[key] = set()
	taxa_dic[key].add(species)
print taxa_dic


	
