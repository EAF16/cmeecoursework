#!/usr/bin/python

"""for loops in python"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports

#constants

#functions

for i in range(5):
	print i
#print variables in the range

my_list = [0, 2, "geronimo!", 3.0, True, False]
for k in my_list:
	print k
#print all objects in the list

total = 0
summands = [0, 1, 11, 111, 1111]
for s in summands:
	print k
#print whether the sum of the terms is equal to 0

#while loops in python
z=0
while z < 100:
	z = z + 1
	print (z)
#print z+1 until it reaches 100

b = True
while b:
	print "GERONIMO! infinite loop! ctrl+c to stop!"
#ctrl + c to stop!

#creates an infinite loop you need to manually exit from
