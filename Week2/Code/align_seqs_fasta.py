#!/usr/bin/python

"""Code for finding the best alignment of two sequences stored in two 
different files. Looks for the greatest number of matches between a 
sequence and another shifted along it. Part of practical 2. TO RUN AS A 
FUNCTION ON FILEs, THEY MUST BE A CSV FILE"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

##IMPORTS

import csv
import sys
import re
import os
from os.path import basename

##FUNCTIONS

#function's purpose is to remove everything but the sequence from the 
#fasta sequence by opening up and reading the specified file. Then a 
#regular expression is used to...
#match the > at the beginning of the file, then any character,
#then continue matching any character until it hits a newline
#then replace all of that with nothing.
#The output of this function will then be the sequence from the file.
def format_FASTA(p):
	"""Isolates a sequence from a FASTA file"""
	f = open(p, "rb")
	readfile = f.read()
	seq = re.sub(r'>.*\n','',readfile).replace('\n', '')
	return seq
	
# function that computes a score
# by returning the number of matches 
# starting from arbitrary startpoint
def calculate_score(s1, s2, l1, l2, startpoint):
	"""Function that computes a score by returning the number of matches
	between two sequences starting from an arbitrary point. Prints an 
	output line showing a * above the area where a match occurs. Requires
	two sequences, their length, and a specified startpoint. Returns 
	the best alignment"""

	# startpoint is the point at which we want to start
	#the matched line will be a series of symbols that shows where the 
	#matched characters are.
	#if none of the positions match, the score will be 0
	matched = "" # contains string for alignement
	score = 0
		
	#For every position on l2,
	#if that position (plus the startpoint being used) isn't beyond the 
	#last position of the longer sequence,
	#and if the letter at the corresponding position in sequnce 1 is the 
	#same,
	#add a * to the symbol line to symbolize a matched base in the matched line
	#add 1 to the match score.
	#Otherwise, add a - to the symbol line and leave the score the same.
	for i in range(l2):
		if (i + startpoint) < l1:
			# if its matching the character
			if s1[i + startpoint] == s2[i]:
				matched = matched + "*"
				score = score + 1
			else:
				matched = matched + "-"

	# build some formatted output
	#Way to visually compare the different alignments.
	#Print the matchedline of . - * that shows how far over the startpoint 
	#moves sequnce 2 and whether there is a match or not at each position.
	#Print sequence 2 shifted over by that particular startpoint
	#Print sequence 1.
	#Print the match score.
	#Print a space to leave a space between different results
	print "." * startpoint + matched           
	print "." * startpoint + s2
	print s1
	print score 
	print ""
			
	return score
	
	#These are test startpoints. Will run calculate_score with startpoints
	#of 0,1,and 5. Later, we will run through every possible startpoint by
	#inputting for i in range(l1):z = calculate_score(s1, s2, l1, l2, i) 
	#which tells python to run through every possible startpoint (i) in the
	#range of the longest sequence.
	calculate_score(s1, s2, l1, l2, 0)
	calculate_score(s1, s2, l1, l2, 1)
	calculate_score(s1, s2, l1, l2, 5)

#This function actually aligns the two sequences from the files provided
def seqs_align_fasta(f,h):
	"""Takes and input of two files and returns the best alignment of
	the two sequences. Output is in file named after the two input files
	"""
	#Extracts the sequences from the two files
	x = format_FASTA(f)
	y = format_FASTA(h)	
	
	#Assigns the file sequences to the variables used within the function
	seq2 = y
	seq1 = x	

	#creates length variables for each sequence
	l1 = len(x)
	l2 = len(y)

	#makes sure s1 and l1 represent the longest sequence by telling python
	#that if l1 is greater than 2, keep the current assignments
	#if not, assign s1 to seq2 and switch l1 with l2 so that way l1, s1, and
	#seq2 all refer to the longest sequence
	if l1 >= l2:
		s1 = seq1
		s2 = seq2
	else:
		s1 = seq2
		s2 = seq1
		l1, l2 = l2, l1 # swap the two lengths
	
	# now try to find the best match (highest score)
	#create empty variables that will be assigned to the best match
	#my_best_align will hold the alignment of s2 that matches best
	#my_best_score will hold the number of matches from the best aligned sequence
	#will explain why it is set at -1 in next group.
	my_best_align = None
	my_best_score = -1

	#for each alignment tested along l1
	#z will be the match score of each alignment (i) possible
	#if z is greater than my_best_score
	#change my_best_align to the alignment currently being tested 
	#changes my_best_score to the new match score.
	#my_best_score is originally set at -1 because it is possible the 
	#sequences will not match at all which would give a score of 0.
	#if my_best_score was set at 0 to start out with, none of the match scores
	#of 0 would displace it and my_best_align would never be filled.
	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z

	#print s2 in the alignment that gives the highest score
	#print s1 below s2 to show the matching alignment
	#read out the best score for the sequnce match shown above
	print my_best_align
	print s1
	print "Best score:", my_best_score

	#These functions strip the two file names of their paths and extensions,
	#respectively, so they can be used to name the new output file
	FileNameA=os.path.basename(f)
	FileNameA=os.path.splitext(FileNameA)[0]
	
	FileNameB=os.path.basename(h)
	FileNameB=os.path.splitext(FileNameB)[0]
	
	#Creates a name for the file that is the first file, underscore,
	#the name of the second file
	FileName = "%s_%s" % (FileNameA, FileNameB)
	
	#Creates a text file with the name constructed above to add the 
	#results to.
	g = open("../Results/%s.txt" % FileName, "wb")
	g.write(my_best_align + '\n')
	g.write(s1 + '\n')
	g.write("Best score: ")
	g.write(str(my_best_score)) 

#Tells the script to run the system arguments in the command line if there
#are any. If there are no arguments specified, it will run on the two fasta
#files specified 
def main(argv):
	#run seqs_align(sys.argv[1],sys.argv[2])
	if len(sys.argv) > 1:
		seqs_align_fasta(sys.argv[1], sys.argv[2])
	else:
		seqs_align_fasta("../Data/fasta/407228326.fasta", "../Data/fasta/407228412.fasta")
	return 0

#Tells the function to go the main function above if the module is running it. 
if (__name__ == "__main__"):
	status = main(sys.argv)
