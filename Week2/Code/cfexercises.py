#!/usr/bin/python

"""Some functions exemplifying the use of control statements. Prints hellos
	and runs foo1, foo2, foo3, foo4, and foo5. Part of practical 2"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports
import sys

#constants

#functions
# How many times will "hello" be printed?


#1)
#prints number of hellos in range (14)
for i in range (3, 17):
	print "hello"


#2)
#prints number of hellos in the range (0 to 11) perfectly divisble by 3
# this apparently includes infinity because 4 hellos are printed
for j in range(12):
	if j % 3 == 0:
		print "hello"


#3)
#in the range of 0-14, if there is 3 left over when dividing by 5, print hello
#if it does not leave a remainder of 3, divide by 4 and print hello if
#there is a remainder of 3
#so there should be 5
for j in range(15): 
	if j % 5 == 3:
		print "hello"
	elif j % 4 == 3:
		print "hello"

#4)
# print hello while z doesn't equal 15. Since z is z + 3 that will be
# 5 hellos for 0,3,6,9,12
z=0
while z !=15:
	print "hello"
	z = z + 3
	


#5)
#starts at 12, never go above 100
#not 31 yet so go to elif
#not 18 yet so do the z=z+1
#keeps going until it hits 18
#prints 1 at 18
#keeps going til it hits 31
#then goes into k loop and prints 0-6 so 7 hellos
#keeps moving til 100
z=12
while z < 100:
	if z == 31:
		for k in range(7):
			print "hello"
	elif z == 18:
		print "hello"
	z = z + 1

# What does fooXX do? Common function name in coding

def foo1(x=160):
	"""raises x to the 0.5 power"""
	return x ** 0.5

def foo2(x,y):
	"""returns the bigger number"""
	if x > y:
		return x
	return y

def foo3 (x, y, z):
	"""If x>y, switches the values. same if y>z."""
	if x > y:
		tmp = y
		y = x
		x = tmp
	if y > z:
		tmp = z
		z = y
		y = tmp
	return [x, y, z]

def foo4(x):
	"""evaluates from 1 to x + 1. Result is 1 * each successive number 
	in range evaluates as result originally = 1 then that result is
	multplied by the first number of the range then that result is 
	multiplied by the next etc."""
	result = 1
	for i in range(1, x + 1):
		result = result * i
	return result

# This is a recursive function, meaning that the function calls itself
# read about it at
# en.wikipedia.org/wiki/Recursion_(computer_science)

def foo5(x):
	"""gives a result that is x times the result of an input of x-1 ex: foo5 (2) will return 2 * 1 because 2 is x and an input of x-1 is 1 which always gives a result of 1"""
	if x == 1:
		return 1
	return x * foo5(x-1)

foo5(10)

#test arguments to show functions running
def main(argv):
	print foo1(5)
	print foo2(5,6)
	print foo3(7,6,5)
	print foo4(5)
	print foo5(5)
	return 0

if (__name__ == "__main__"):
	status = main(sys.argv)
	sys.exit(status)
