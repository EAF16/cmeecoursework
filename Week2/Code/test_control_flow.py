#!/usr/bin/python

"""Using the even_or_odd function to try unit testing with doctest"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports
import sys 
import doctest

#constants

#functions
def even_or_odd(x=0):
	#each of these tests gives a sample input and the expected output
	#If the expected and given outputs don't match, the function
	#will fail that test.
	"""Find whether a number x is even or odd.
	
	>>> even_or_odd(10)
	'10 is Even!'
	
	>>> even_or_odd(5)
	'5 is Odd!'
	
	Whenever a float is provided, then the closest integer is used:
	>>> even_or_odd(3.2)
	'3 is Odd!'
	
	in case of negative numbers, the positive is taken:
	>>> even_or_odd(-2)
	'-2 is Even!'
	
	"""
	#Define function to be tested
	#If the number divided by two gives no remainder, it is even
	if x % 2 == 0: #The conditional if
		return "%d is Even!" % x
	return "%d is Odd!" % x

# # Why surpress this block? Not necessary for test
# ctrl + e to surpress whole block
#~ def main(argv):
	#~ print even_or_odd(22)
	#~ print even_or_odd(33)
	#~ return 0

# if (__name__== "__main__"):
#	status = main(sys.argv)
#	sys.exit(status)

doctest.testmod() # To run with embedded tests
