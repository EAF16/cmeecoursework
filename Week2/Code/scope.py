#!/usr/bin/python

"""This is a practice for using global variables"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

##Try this first

_a_global = 10

def a_function() :
	"""_a_global and a_local are defined in function and running it will
	print their values"""
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

# _a_global and _a_local have been defined within the function so will
# print as the values defined there

a_function()
print "Outside the function, the value is ", _a_global

# here, the printed value will be the one defined earlier (10) since we
# are outside the function

## Now try this

_a_global = 10

def a_function() :
	"""more practice with global variables. uses the global variable
	defined elsewhere"""
	global _a_global
	_a_global = 5
	_a_local = 4
	print "Inside the function, the value is ", _a_global
	print "Inside the function, the value is ", _a_local
	return None

a_function()
print "Outside the function, the value is ", _a_global

# now, even outside the function _a_global is 5 because it has been
# defined as a global variable
