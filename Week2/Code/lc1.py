#!/usr/bin/python

"""Seperating a tuple of bird species into lists of latin names, common 
names, and mean body masses using list comprehensions and conventional 
loops. Part of practical 2"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports

#constants

#functions

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )

#(1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can choose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS



#(1) List Comprehensions

# latin_names_lc = list : creates a list called latin_names_lc
# The text in brackets tells python to fill that list with
# the first part of each tuple entry for each tuple entry (i) in birds
latin_names_lc = list (i[0] for i in birds)
print latin_names_lc

# common_names_lc = list : creates a list called common_names_lc
# The text in brackets tells python to fill that list with
# the second part of each tuple entry for each tuple entry (i) in birds
common_names_lc = list (i[1] for i in birds)
print common_names_lc

# mean_body_mass_lc = list : creates a list called mean_body_mass_lc
# The text in brackets tells python to fill that list with
# the third part of each tuple entry for each tuple entry (i) in birds
mean_body_mass_lc = list (i[2] for i in birds)
print mean_body_mass_lc



#(2) Coventional loops

latin_names_loop = []					# Create empty list
for i in birds:							# For tuple entry (i) in birds...
	latin_names_loop.append(i[0])		# Add first part of each tuple entry to new list
print latin_names_loop					# Print new list

common_names_loop = []					# Create empty list
for i in birds:							# For tuple entry (i) in birds...
	common_names_loop.append(i[1])		# Add second part of each tuple entry to new list
print common_names_loop					# Print new list

mean_body_mass_loop = []				# Create empty list
for i in birds:							# For tuple entry (i) in birds...
	mean_body_mass_loop.append(i[2])	# Add third part of each tuple entry to new list
print mean_body_mass_loop				# Print new list
