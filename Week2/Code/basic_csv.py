#!/usr/bin/python

"""Reads testcsv.csv and lists species by name then creates a document 
listing species and their weight"""

___author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

##IMPORTS

import csv

# Read a file containing:
# 'Species', 'Infraorder', 'Family', 'Distribution', 'Body mass male (Kg)'
f = open("../Sandbox/testcsv.csv", "rb")

csvread = csv.reader(f)
temp = []

#Goes through each row and makes each row an inseperable tuple in the 
#temporary set then prints the row and a line indicating what the 
#species is
for row in csvread:
	#keep sets together
	temp.append(tuple(row))
	print row
	print "The species is", row[0]
	
f.close()

# write a file containing only species name and body mass
#Opens the file with the data and creates one for the output
f = open("../Sandbox/testcsv.csv", "rb")
g = open("../Sandbox/bodymass.csv", "wb")

#reads and writes in csv format
csvread = csv.reader(f)
csvwrite = csv.writer(g)

#Print the row then add the first and fifth element to the new write file
for row in csvread:
	print row
	csvwrite.writerow([row[0], row[4]])

f.close()
g.close()


