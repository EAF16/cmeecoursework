#!/usr/bin/python

"""Some functions exemplifying the use of control statements including
	even_or_odd, largest_divisor_five, is_prime, find_all_primes"""
#docstrings are considered part of the running code (normal comments are
#stripped). Hence, you can access your docstrings at run time.

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

#imports
import sys 

#constants

#functions
def even_or_odd(x=0): # if not specified, x should take value 0
	"""Find whether a number x is even or odd by checking if there is a 
	remainder after dividing by 2"""
	if x % 2 == 0: #The conditional if
		return "%d is Even!" % x
	return "%d is Odd!" % x

def largest_divisor_five(x=120):
	"""Find which is the largest divisor of x among 2,3,4,5 by checking if 
	#there is a remainder after dividing by each of them"""
	largest = 0
	if x % 5 == 0:
		largest = 5
	elif x % 4 == 0: #means "else, if"
		largest = 4
	elif x % 3 == 0:
		largest = 3
	elif x % 2 == 0:
		largest = 2
	else: #When all oter (if, elif) conditions are not met
		return "No divisor found for %d!" % x #each function can return 
					#a value or a variable
	return "The largest divisor of %d is %d" % (x, largest)

def is_prime(x=70):
	"""Tells whether each of the numbers between 2 and the input is a prime 
	or not by dividing x by integers to see if it is evenly divisible
	prints all the divisors found"""
		for i in range(2,x): #"range" returns a sequence of integers.
		if x % i == 0:
			print "%d is not a prime: %d is a divisor" % (x,i) #print formatted
					# text "%d %s %f %e" % (20, "30", 0.0003, 0.00003)
			return False
	print "%d is a prime!" % x
	return True

def find_all_primes(x=22):
	"""Creates a set called allprimes and adds prime numbers in the range 
	to it using is_prime for each number in the range. Feeds out the 
	number of primes in the range as well as a set of each of the primes.
	"""
	allprimes = []
	for i in range(2, x + 1):
		if is_prime(i):
			allprimes.append(i)
	print "There are %d primes between 2 and %d" % (len(allprimes), x)
	return allprimes

def main(argv):
	#Main arguments if the module is being run. Each function has its own
	#default argument above if it's being run by itself and not given an 
	#input.
	print even_or_odd(22)
	print even_or_odd(33)
	print largest_divisor_five(120)
	print largest_divisor_five(121)
	print is_prime(60)
	print is_prime(59)
	print find_all_primes(100)
	return 0

if (__name__== "__main__"):
	status = main(sys.argv)
	sys.exit(status)
	# sys.exit ("don't want to do this right now!")

