#!/usr/bin/python
# Filename: using_name.py

"""This exemplifies the purpose of __name__ and __main__"""

__author__ = "Emma Fox (e.fox16@imperial.ac.uk)"
__version__ = "0.0.1"

if __name__ == "__main__":
		print "This program is being run by itself"
else:
		print "I am being imported from another module"
		
# If name=main means that if it is being called and run by itself, then do this 
# If it is being called by another function, do what is specified in the command line
