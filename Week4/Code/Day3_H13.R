#FileName: Day3_H13.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This script includes the practice for fitting linear models 

##########  H13 ##########

rm(list = ls())

d<-read.table("../Data/SparrowSize.txt", header = TRUE)

#Trying out an ANOVA
d1<-subset(d, d$Wing!="NA")
#visualizing the data
summary(d1$Wing)
hist(d1$Wing)

#Modelling wing and sex with a linear model
model1<-lm(Wing~Sex.1, data=d1)
print(summary(model1))
#sex is significant, males have longer wings
#visualizing the difference
boxplot(d1$Wing~d1$Sex.1, ylab="Wing length (mm)")

#Modelling the effect of sex on wing length with an ANOVA
print(anova(model1))

#post-hoc test to determine effect size
t.test(d1$Wing~d1$Sex.1, var.equal=TRUE)

#TESTING THE EFFECT OF BIRDID (AS A GROUPING FACTOR) ON WING LENGTH
#turning birdid into a factor
str(d1)
d1$BirdID<-as.factor(d1$BirdID)
str(d1)

model2<-lm(Wing~BirdID, data = d1)
print(anova(model2))
print(summary(model2))
boxplot(d1$Wing~d1$BirdID, ylab="Wing length (mm)")
#the boxplot and linear model have hundreds of groupings
#the anova looks much more clear and understandable

#Trying with dplyr
require(dplyr)
tbl_df(d1)
glimpse(d1)

#Counting the number of single, double, triple observations etc.
print(d$Mass %>% cor.test(d$Tarsus, na.rm=TRUE))
d1 %>% 
  group_by(BirdID) %>%
  summarise(count=length(BirdID))
print(count(d1, BirdID))

#Shortcut
count(d1, BirdID)

#Before, it had been listing how many times each bird was measured
#This now shows how many birds were measured once, twice, etc. 
print(d1 %>% 
    group_by(BirdID) %>%
    summarise(count=length(BirdID)) %>%
      count(count))

# #If we had done it like this:
# count(d1, d1$Bird) %>%
#   count(count)
# #the function and variable names would have clashed

#Testing whether the within or among group variance is greater for wing length by BirdID
model3<-lm(Wing~as.factor(BirdID), data = d1)
anova(model3)

#How to figure out the difference in values when multiple groups are involved
boxplot(d$Mass~d$Year)
m2<-lm(d$Mass~as.factor(d$Year))
anova(m2)
#there is a difference but between which years?
summary(m2)
#visualizing the matrix
t(model.matrix(m2))

#EXERCISES#
#1) Table of repeated wing length within individuals
