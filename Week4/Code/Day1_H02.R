#FileName: Day1_H02.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This script explores a data set by plotting histograms and calculating mean, median, mode, sum of squares,
#variance, and standard deviation for the sparrow data. Also introduces how to deal with NA values, including subsetting. 
#Some work with z scores, distributions, and quantiles. 
##########  H02 ##########

#Clear environment
rm(list = ls())

#setwd("/home/cmee13/Documents/SparrowStats/")

#Bringing in a data set
d <- read.table("../Data/SparrowSize.txt", header = TRUE)
str(d)
names(d)
head(d)

#Checking the distribution
length(d$Tarsus) #1770

#Histogram
hist(d$Tarsus)

#mean
mean(d$Tarsus) 
help(mean)
#Strips NAs before calculation
mean(d$Tarsus, na.rm = TRUE)
#Median
median(d$Tarsus, na.rm = TRUE)
#Mode
mode(d$Tarsus)

#Histogram Set
#will get gaps where the precision is too great
#compared to what you have 
#make sure you're using a sensible amount of decimal places
par(mfrow = c(2,2))
hist(d$Tarsus, breaks = 3, col="grey")
hist(d$Tarsus, breaks = 10, col="grey")
hist(d$Tarsus, breaks = 30, col="grey")
hist(d$Tarsus, breaks = 100, col="grey")

#Trying to find the mode
#install.packages("modeest")
require(modeest)
?modeest
?mlv
#mlv(d$Tarsus)
#Will give an error since there are missing values

#Subsetting so we don't have NAs
d2<-subset(d, d$Tarsus!="NA")
length(d$Tarsus) #1770 (number of observations)
length(d2$Tarsus) #1685

mlv(d2$Tarsus)
#these error messages make sense so far so we ignore
#continuous values in R are hard to find a most repeated value for

#Summary statistics with NA removed
mean(d$Tarsus, na.rm = TRUE) #18.52335
median(d$Tarsus, na.rm = TRUE) #18.6
mlv(d2$Tarsus)

#Range, variance, and standard deviation
#Comparing between full set and subset with na.rm
range(d$Tarsus, na.rm = TRUE)
range(d2$Tarsus, na.rm = TRUE)
var(d$Tarsus, na.rm = TRUE)
var(d2$Tarsus, na.rm = TRUE)

#Long hand variance
#Actual formula calculations
sum((d2$Tarsus - mean(d2$Tarsus))^2)/(length(d2$Tarsus)-1)
sqrt(var(d2$Tarsus))
sd(d2$Tarsus)

#LECTURE EXERCISE
#Tarsus
mean(d$Tarsus, na.rm = TRUE)
var(d$Tarsus, na.rm = TRUE)
sd(d$Tarsus, na.rm = TRUE)
#Bill Length
mean(d$Bill, na.rm = TRUE)
var(d$Bill, na.rm = TRUE)
sd(d$Bill, na.rm = TRUE)
#Body Mass
mean(d$Mass, na.rm = TRUE)
var(d$Mass, na.rm = TRUE)
sd(d$Mass, na.rm = TRUE)
#Wing Length
mean(d$Wing, na.rm = TRUE)
var(d$Wing, na.rm = TRUE)
sd(d$Wing, na.rm = TRUE)
#Graph
par(mfrow = c(2,2))
hist(d$Tarsus)
hist(d$Bill)
hist(d$Mass)
hist(d$Wing)

#2001
d2001 <- subset(d, d$Year == "2001")
#Tarsus
mean(d2001$Tarsus, na.rm = TRUE)
var(d2001$Tarsus, na.rm = TRUE)
sd(d2001$Tarsus, na.rm = TRUE)
#Bill Length
mean(d2001$Bill, na.rm = TRUE)
var(d2001$Bill, na.rm = TRUE)
sd(d2001$Bill, na.rm = TRUE)
#Body Mass
mean(d2001$Mass, na.rm = TRUE)
var(d2001$Mass, na.rm = TRUE)
sd(d2001$Mass, na.rm = TRUE)
#Wing Length
mean(d2001$Wing, na.rm = TRUE)
var(d2001$Wing, na.rm = TRUE)
sd(d2001$Wing, na.rm = TRUE)
#Graph
par(mfrow = c(2,2))
hist(d2001$Tarsus)
#hist(d2001$Bill)
#THERE ARE NO VALUES in the above
hist(d2001$Mass)
hist(d2001$Wing)


#Z scores
zTarsus <- (d2$Tarsus - mean(d2$Tarsus))/sd(d2$Tarsus)
var(zTarsus) #1
sd(zTarsus) #1
hist(zTarsus)
#can do scale(d2$Tarsus, center = TRUE, scale = TRUE)

#Making data set from scratch in a set distribution
set.seed(123)
znormal <- rnorm(1e+06)
hist(znormal, breaks = 100)
summary(znormal)

#Normal distribution from random samples
#like above
#gives the 2.5% and 97.5% quantile
qnorm(c(0.025, 0.975))
#pnorm(.Last.value)

par(mfrow = c(1,2))
hist(znormal, breaks = 100)
abline(v = qnorm(c(0.25, 0.5, 0.75)), lwd = 2)
abline(v = qnorm(c(0.025, 0.975)), lwd = 2, lty = "dashed")
plot(density(znormal))
abline(v = qnorm(c(0.25, 0.5, 0.75)), col = "gray")
abline(v = qnorm(c(0.025, 0.975)), lty = "dotted", col = "black")
abline(h = 0, lwd = 3, col = "blue")
text(2, 0.3, "1.96", col = "red", adj = 0)
text(-2, 0.3, "-1.96", col = "red", adj = 1)

par(mfrow = c(1,1))
boxplot(d$Tarsus~d$Sex.1, col = c("red", "blue"), ylab="Tarsus length (mm)")
