readme for Week4 Directory

├── Code
│   ├── Day1_H01.R - This script tries out basic commands with the calculator, assigning variables, and making vectors.
│   ├── Day1_H02.R - This script explores a data set by plotting histograms and calculating mean, median, mode, sum of squares,variance, and standard deviation for the sparrow data. Also introduces how to deal with NA values, including subsetting. Some work with z scores, distributions, and quantiles.
│   ├── Day1_L4.R - This script works with the calculations used for standard error
│   ├── Day2_L5.R - This script works with the calculations used for t.tests and their interpretation
│   ├── Day3_H10.R - This script is used to practice using linear models to describe relationships including using diagnostics to test for normal distribution of residuals.
│   ├── Day3_H13.R - This script includes the practice for fitting linear models 
│   ├── Day3_L09.R - Exploring our first basic linear model
│   ├── Day4_H14.R - This script is used to run repeatability calculations on bird wing length and mass by ID.
│   ├── Day4_H16.R - This script is the full 11 steps for running mixed models and multiple regressions
│   ├── Day4_H17.R - This script runs through the calculations for a chi square test as well as the shorthand function. 
│   └── Day4_H18.R - This script is runs through the calculations for a chi square test as well as the shorthand function.
├── Data
│   ├── daphnia.txt - Data set for practice with mixed models
│   ├── ipomopsis.txt - Part of an exercise that required us to go through the 11 steps to analyze data create multiple regression
│   ├── ObserverRepeatability.txt - Data set for testing the repeatability of our measures of sparrow leg
│   ├── SparrowSize.txt - The main data set from Dr. Schroeder's project which we used for nearly all the handouts
│   └── timber.txt - First practice set for multiple regression
└── Results
