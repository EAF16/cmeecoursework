readme for Week5 Directory

├── Code
│   ├── clifford.test.R - Code provided to run a function that tests for the correlation between species richness and mean AET in the spatial methods practical.
│   ├── UK_zonalstats.py - This script takes bioclim temperature and precipation maps and formats them to be merged with a land cover data set so the UK means of each land cover classification can be extracted (full script provided beforehand and subsequently edited by the student).
│   └── Spatial_methods_practical.R - This script looks at the effect of mean elevation, average annual temperature, and actual evapotranspiration on bird species richness in Africa and checks for spatial correlation using Moran's I calculations, local indicators of spatial autocorrelation, and correlograms
├── Data
│   ├── avian_richness.tif - Bird species richness data in Africa (used for spatial methods practical)
│   ├── bio1_15.tif - Mean annual temperature data for western UK grid cells (used for UK zonalstats calculations practical)
│   ├── bio1_16.tif - Mean annual temperature data for eastern UK grid cells (used for UK zonalstats calculations practical)
│   ├── bio12_15.tif - Total annual precipitation data for western UK grid cells (used for UK zonalstats calculations practical)
│   ├── bio12_16.tif - Total annual precipitation data for eastern UK grid cells (used for UK zonalstats calculations practical)
│   ├── elev.tif - Mean elevation in African grid-cells (used for spatial methods practical)
│   ├── g250_06.tif - Land classification data (used for UK zonalstats calculation practical)
│   ├── mean_aet.tif - Average annual temperature for African grid-cells (used for spatial methods practical)
│   └── mean_temp.tif - Average annual evapotranspirations for African grid-cells (used for spatial methods practical)
└── Results
    ├── 01_Histogram_of_4Variables.pdf - 01 to 11 are plots from the spatial methods practical. Description name is in the title.
    ├── 02_Map_of_4Variables.pdf
    ├── 03_Map_of_SpeciesRichness.pdf
    ├── 04_Variables_Effect_on_SpeciesRichness.pdf
    ├── 05_lisa_plots_for_SpeciesRichness.pdf
    ├── 06_lisa_plots_for_Temp.pdf
    ├── 07_Spatial_Autocorrelation_Maps.pdf
    ├── 08_SAR_Residuals.pdf
    ├── 09_Correlogram_of_SpeciesRichness.pdf
    ├── 10_Effect_of_ClassMean.pdf
    ├── 11_Comparing_Residual_Autocorrelation.pdf
    └── zonalstats.csv -Output table from UK zonalstats calculation practical. Table of mean temperatures and precipiation for each land class.
Results
    └── zonalstats.csv - 
	├── sSPpatial_methods_plots.pdf - Plots from the spatial methods practical exploring autocorrelation
