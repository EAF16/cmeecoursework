\documentclass[11pt]{article}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
\usepackage{indentfirst}
\usepackage{graphicx, setspace}
\usepackage{lineno}
\usepackage[T1]{fontenc}


\renewcommand{\rmdefault}{phv} % Arial
\renewcommand{\sfdefault}{phv} % Arial

\linespread{1.25}

\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear, open={(}, close={)}}

\newcommand{\HRule}[1]{\rule{\linewidth}{#1}}

\title{	\huge \textsc{Proposal}
		\\ [3.0cm]
		\HRule{1pt} \\
		\LARGE \textbf{Computational methods for detecting adaptive evolution from high-throughput sequencing data}
		\HRule{1pt} \\ [0.2cm]
		\Large \textsc{Emma Fox}
		\\ [9.0cm]}


\author{ 
		Supervised by Dr. Matteo Fumagalli \\
		Research Fellow, Department of Life Sciences (Silwood Park) \\
		 Imperial College London \\
		(m.fumagalli@imperial.ac.uk) \\ \\
		Committee:  Dr. Jason Hodgson and  Professor Alfried Vogler
		}

\date{}

\begin{document}

\maketitle

\newpage
\begin{linenumbers}

\section{Introduction}

The aim of this project is to develop computational refinements to the analysis of next generation sequencing (NGS) data that will account for the uncertainty associated with low sequencing depth when detecting evolution between populations. Sequencing depth is often decreased in favour of maximising the number of individuals sampled and, subsequently, the amount of variation represented in the study 
\citep{nielsen2011genotype}. Using less reads, though, lowers the accuracy of sequence alignment as well as base, Single Nucleotide Polymorphism (SNP), and genotype calling \citep{nielsen2011genotype}. This will then decrease the meaningfulness of conclusions drawn from the analysis.   

Detecting adaptive evolution using genetic data can be done by comparing genomes to find loci or regions where allele frequencies differ significantly between populations \citep{vitti2013detecting}. These areas are assigned fixation index (FST) values which indicate the degree of structuring between population at that locus \citep{bhatia2013estimating}. The Population Branch Statistic (PBS) uses these FST values to determine the evolutionary distance and the direction of evolution between a focus population and a pair of reference populations\citep{yi2010sequencing}. This structuring of allele frequencies can also be sumamrised using a site frequency spectrum (SFS) which contains the joint distribution of allele frequencies at the genome or locus level for multiple populations. The region around a gene selection acts on will show depressed variation within a population and inflated interpopulation diversity \citep{vitti2013detecting}. The ability to visualise these selective sweeps makes SFS a useful tool for detecting natural selection.   

Previous research on increasing the accuracy of data used in these calculations has incorporated genotype likelihoods for estimating summary statistics, rather than simply using assignments that cleared a threshold of certainty \citep{nielsen2011genotype,fumagalli2013quantifying}. Prior knowledge of linkage disequilibrium patterns and allele frequencies can also be incorporated with genotype likelihoods to give posterior probabilities which can be used in calculations involving the called variables \citep{kim2011estimation}. \cite{nielsen2005genomic} employed a composite likelihood method which used patterns in neutral areas of the genome to set the null hypothesis potential selective sweeps would be compared to. Using probabilities rather than simply working with the plausible identity takes advantage of the full range of information available to come to the most accurate conclusion. 

\section{Proposed Methods}
The first phase of the project will involve modelling hypothetical data by simulating genomes using the SLiM software \citep{haller2016slim}. I will then compare patterns detected in the 'true' data with those detected when only a subsample of the 'true' sequencing reads are used. The next phase will work on developing a technique, using the ANGSD package \citep{korneliussen2014angsd} and ngsTools platform \citep{fumagalli2014ngstools}, to increase the accuracy of the low-sample-depth analysis by including genotype likelihood data. I will work in collaboration with Dr Thorfinn Korneliussen from the Natural History Museum of Denmark in Copenhagen for the development and implementation of the proposed computational method. The error between the true data and this method, or conventional models, will then be assessed and quantified through extensive simulations. The final steps will use this new method to evaluate SFS patterns in both publicly available human genomes \citep{10002015global} and genomes from a wild population of Marsican brown bears. Access to the bear genomes will be possible through my proposed collaboration with Professor Giorgio Bertorelle, University of Ferrara, Italy. 


\section{Anticipated Outputs and Outcomes}
The purpose of this project is to generate and apply a new computational method that detects adaptive evolution by comparing trends of interpopulation variation from low-coverage sequencing data. After extensive testing, the method developed will be compiled into an open-access free-source software package. A further goal is to prepare an application note manuscript for publication (possibly for submission to BMC Bioinformatics or Molecular Ecology Resources). This manuscript will include an application component testing the new method on data sets of human and Marsican brown bear genomes.  

\section{Project Feasibility}
This project builds on a previously proposed computational framework by extending it for further applications \citep{fumagalli2013quantifying}. It is therefore designed to be reasonable enough to be completed in the time available. The first phase is comprised of instructive training in each of the software packages. The second phase involves simulating data which will be used for method development and error quantification phases. The final two phases will apply the techniques developed to the two available data sets. The time-frame of each phase is shown in Figure 1. 

\begin{figure}[htp]
\centering
\includegraphics[scale=0.20]{Gantt_Chart.jpg}
\caption{Gantt chart of project timeline}
\label{}
\end{figure}

\section{Budget}
I would like to request use of the \pounds500 grant available for all thesis projects in order to visit project collaborators based in Denmark to consult on the modelling component or collaborators based in Italy to consult on the empirical component. These expenses include up to \pounds150-\pounds250 for flights and other transport as well as roughly \pounds200 for lodging. These visits would give me the opportunity to develop my project with top-tier researchers in the field of bioinformatics and conservation genetics. 

\end{linenumbers}

\bibliography{Proposal.bib}

\end{document}
