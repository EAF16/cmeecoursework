#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: csvtospace.sh
# Desc: substitute the commas in the files with spaces
#		saves the output into a .txt file
# Arguments: 1-> comma-separated file 
# Date: 4 Oct 2016

echo "Creating a space delimited version of $1 ..."

cat $1 | tr -s "," " " >> $1.txt

echo "Done!"

exit
