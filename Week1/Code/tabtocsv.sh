#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the tabs in the files with commas
#		saves the output into a .csv file
# Arguments: 1-> tab delimited file
# Date: 4 Oct 2016

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit
