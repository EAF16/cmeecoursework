readme for Week1 Directory

***************
 DATA FOLDER:
***************
spawannxs.txt - List containing protected species names. Used to practice 
					searching with grep.

*Fasta Folder
407228412.fasta - Contains a Homo sapiens tryptase beta 2 gene sequence. 
					Used to test practical set 1 code.
407228326.fasta - Contains a Homo sapiens x Mus musculus hybrid cell line 
					gene sequence. Used to test practical set 1 code.
E.coli.fasta - Contains an E. coli gene sequence. Used to test practical 
				set 1 code. 

*Temperatures Folder
1800.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1801.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1802.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1803.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1800.csv.txt - Space delimited file created with csvtospace.sh.
1801.csv.txt - Space delimited file created with csvtospace.sh.
1802.csv.txt - Space delimited file created with csvtospace.sh.
1803.csv.txt - Space delimited file created with csvtospace.sh.

***************
 CODE FOLDER:
***************
boilerplate.sh - Prints "This is a shell script".

tabtocsv.sh - Converts a tab delimited file to a new comma delimited file. 

variables.sh - Exemplifies assigning variables. Changes a variable from 
				"some string" to the value input. Also adds two entered
				numbers. 

MyExampleScript.sh - Greets user.

CountLines.sh - Counts the number of lines in the file specified.

ConcatenateTwoFiles.sh - Combines two files into a third, specified file
					and reads out the name of the new file.
					
FirstExample.tex - File typed up and used to compile LaTeX document.

FirstBiblio.bib - BibTeX file used for LaTeX document (Einstein 1905).

CompileLaTeX.sh - Shell script used to create a LaTeX PDF with citation.

FirstExample.pdf - LaTeX PDF created using CompileLaTeX.sh from FirstExample.tex

FirstExample.bbl - Extraneous file created by CompileLaTeX.sh when compiling PDF.

FirstExample.blg - Extraneous file created by CompileLaTeX.sh when compiling PDF.

csvtospace.sh - For practical 1 task. Converts a comma delimited file
				to a space delimited file. 

UnixPrac1.txt - Text for UNIX shell commands to complete the practical 1 tasks.


***************
 SANDBOX FOLDER:
***************
test.txt - Created to practice redirecting outputs to files. ALso used to test tabtocsv.sh

ListRootDir.txt - Saved a list of commands in root directory >>. 

*TestWild Folder
File1.txt - Created as part of a set to test wildcard commands.
File2.txt - Created as part of a set to test wildcard commands.
File3.txt - Created as part of a set to test wildcard commands.
File4.txt - Created as part of a set to test wildcard commands.
File1.csv - Created as part of a set to test wildcard commands.
File2.csv - Created as part of a set to test wildcard commands.
File3.csv - Created as part of a set to test wildcard commands.
File4.csv - Created as part of a set to test wildcard commands.
Anotherfile.txt - Created as part of a set to test wildcard commands.
Anotherfile.csv - Created as part of a set to test wildcard commands.

*TestFind Folder - Created to practice with the find command.
	Dir1
		Dir11
			Dir111
				File111.txt
		File1.txt
		File1.tex
		File1.csv
	Dir2
		File2.txt
		File2.tex
		File2.csv
	Dir3 
		File3.txt

test.txt.csv - Result from running tabtocsv.sh on test.txt. 

