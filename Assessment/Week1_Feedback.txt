Starting weekly assessment for Emma, Week1

Current Marks = 100

Note that: 
(1) Major sections begin with a double "====" line 
(2) Subsections begin with a single "====" line 
(3) Code output or text file content are printed within single "*****" lines 

======================================================================
======================================================================
PART 1: Checking project workflow...

Found the following directories in parent directory: Week1, Assessment, Week2, .git

Found the following files in parent directory: README.txt, .gitignore

Checking for key files in parent directory...

Found .gitignore in parent directory, great! 

Printing contents of .gitignore:
**********************************************************************
*~
*.tmp
**********************************************************************

Found README in parent directory, named: README.txt

Printing contents of README.txt:
**********************************************************************
My CMEE 2016-17 Coursework Repository
Do I like this better?
**********************************************************************

======================================================================
Looking for the weekly directories...

Found 2 weekly directories: Week1, Week2

The Week1 directory will be assessed 

======================================================================
======================================================================
PART 2: Checking weekly code and workflow...

======================================================================
Assessing WEEK1...

Found the following directories: Code, Data, Sandbox

Found the following files: readme.txt

Checking for readme file in weekly directory...

Found README in parent directory, named: readme.txt

Printing contents of readme.txt:
**********************************************************************
readme for Week1 Directory

***************
 DATA FOLDER:
***************
spawannxs.txt - List containing protected species names. Used to practice 
					searching with grep.

*Fasta Folder
407228412.fasta - Contains a Homo sapiens tryptase beta 2 gene sequence. 
					Used to test practical set 1 code.
407228326.fasta - Contains a Homo sapiens x Mus musculus hybrid cell line 
					gene sequence. Used to test practical set 1 code.
E.coli.fasta - Contains an E. coli gene sequence. Used to test practical 
				set 1 code. 

*Temperatures Folder
1800.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1801.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1802.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1803.csv - File to run with csvtospace.sh to create a new file that is space delimited.
1800.csv.txt - Space delimited file created with csvtospace.sh.
1801.csv.txt - Space delimited file created with csvtospace.sh.
1802.csv.txt - Space delimited file created with csvtospace.sh.
1803.csv.txt - Space delimited file created with csvtospace.sh.

***************
 CODE FOLDER:
***************
boilerplate.sh - Prints "This is a shell script".

tabtocsv.sh - Converts a tab delimited file to a new comma delimited file. 

variables.sh - Exemplifies assigning variables. Changes a variable from 
				"some string" to the value input. Also adds two entered
				numbers. 

MyExampleScript.sh - Greets user.

CountLines.sh - Counts the number of lines in the file specified.

ConcatenateTwoFiles.sh - Combines two files into a third, specified file
					and reads out the name of the new file.
					
FirstExample.tex - File typed up and used to compile LaTeX document.

FirstBiblio.bib - BibTeX file used for LaTeX document (Einstein 1905).

CompileLaTeX.sh - Shell script used to create a LaTeX PDF with citation.

FirstExample.pdf - LaTeX PDF created using CompileLaTeX.sh from FirstExample.tex

FirstExample.bbl - Extraneous file created by CompileLaTeX.sh when compiling PDF.

FirstExample.blg - Extraneous file created by CompileLaTeX.sh when compiling PDF.

csvtospace.sh - For practical 1 task. Converts a comma delimited file
				to a space delimited file. 

UnixPrac1.txt - Text for UNIX shell commands to complete the practical 1 tasks.


***************
 SANDBOX FOLDER:
***************
test.txt - Created to practice redirecting outputs to files. ALso used to test tabtocsv.sh

ListRootDir.txt - Saved a list of commands in root directory >>. 

*TestWild Folder
File1.txt - Created as part of a set to test wildcard commands.
File2.txt - Created as part of a set to test wildcard commands.
File3.txt - Created as part of a set to test wildcard commands.
File4.txt - Created as part of a set to test wildcard commands.
File1.csv - Created as part of a set to test wildcard commands.
File2.csv - Created as part of a set to test wildcard commands.
File3.csv - Created as part of a set to test wildcard commands.
File4.csv - Created as part of a set to test wildcard commands.
Anotherfile.txt - Created as part of a set to test wildcard commands.
Anotherfile.csv - Created as part of a set to test wildcard commands.

*TestFind Folder - Created to practice with the find command.
	Dir1
		Dir11
			Dir111
				File111.txt
		File1.txt
		File1.tex
		File1.csv
	Dir2
		File2.txt
		File2.tex
		File2.csv
	Dir3 
		File3.txt

test.txt.csv - Result from running tabtocsv.sh on test.txt. 

**********************************************************************

Results directory missing!

Found 12 code files: ConcatenateTwoFiles.sh, .txt, CountLines.sh, variables.sh, CompileLaTeX.sh, csvtospace.sh, FirstExample.tex, MyExampleScript.sh, FirstBiblio.bib, UnixPrac1.txt, tabtocsv.sh, boilerplate.sh

Found the following extra files: FirstExample.pdf, .csv, FirstExample.blg, .log, FirstExample.bbl
What are they doing here?! 

0.5 pt deducted per extra file

Current Marks = 97.5

======================================================================
Testing script/code files...

======================================================================
Inspecting script file ConcatenateTwoFiles.sh...

File contents are:
**********************************************************************
#!/bin/bash
cat $1 > $3
cat $2 >> $3
echo "Merged File is"
cat $3
**********************************************************************

Testing ConcatenateTwoFiles.sh...

Output (only first 500 characters): 

**********************************************************************
Merged File is

**********************************************************************

Encountered error:
ConcatenateTwoFiles.sh: line 2: $3: ambiguous redirect
ConcatenateTwoFiles.sh: line 3: $3: ambiguous redirect

======================================================================
Inspecting script file .txt...

File contents are:
**********************************************************************
**********************************************************************

Testing .txt...

======================================================================
Inspecting script file CountLines.sh...

File contents are:
**********************************************************************
#!/bin/bash
NumLines=`wc -l < $1`
echo "The file $1 has $NumLines lines"
echo
**********************************************************************

Testing CountLines.sh...

Output (only first 500 characters): 

**********************************************************************
The file  has  lines


**********************************************************************

Encountered error:
CountLines.sh: line 2: $1: ambiguous redirect

======================================================================
Inspecting script file variables.sh...

File contents are:
**********************************************************************
#!/bin/bash
# shows the use of variables

MyVar='some string'
echo 'the current value of the variable is' $MyVar
echo 'Please enter a new string'
read MyVar
echo 'the current value of the variable is' $MyVar
##Reading multiple values
echo 'Enter two numbers separated by space(s)'
read a b 
echo 'you entered' $a 'and' $b '. Their sum is:'
mysum=`expr $a + $b`
echo $mysum
**********************************************************************

Testing variables.sh...

Output (only first 500 characters): 

**********************************************************************
the current value of the variable is some string
Please enter a new string
the current value of the variable is
Enter two numbers separated by space(s)
you entered and . Their sum is:


**********************************************************************

Encountered error:
expr: syntax error

======================================================================
Inspecting script file CompileLaTeX.sh...

File contents are:
**********************************************************************
#!/bin/bash
pdflatex $1.tex
pdflatex $1.tex
bibtex $1
pdflatex $1.tex
pdflatex $1.tex
evince $1.pdf &

## cleanup
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
**********************************************************************

Testing CompileLaTeX.sh...

Output (only first 500 characters): 

**********************************************************************
This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (preloaded format=pdflatex)
 restricted \write18 enabled.
entering extended mode
(/usr/share/texlive/texmf-dist/tex/latex/tools/.tex
LaTeX2e <2016/02/01>
Babel <3.9q> and hyphenation patterns for 3 language(s) loaded.
File ignored)
*
! Emergency stop.
<*> .tex
        
!  ==> Fatal error occurred, no output PDF file produced!
Transcript written on .log.
This is pdfTeX, Version 3.14159265-2.6-1.40.16 (TeX Live 2015/Debian) (prel
**********************************************************************

Encountered error:
bibtex: Need exactly one file argument.
Try `bibtex --help' for more information.
rm: cannot remove '*~': No such file or directory
rm: cannot remove '*.aux': No such file or directory
rm: cannot remove '*.dvi': No such file or directory
rm: cannot remove '*.log': No such file or directory
rm: cannot remove '*.nav': No such file or directory
rm: cannot remove '*.out': No such file or directory
rm: cannot remove '*.snm': No such file or directory
rm: cannot remove '*.toc': No such file or directory

** (evince:17409): WARNING **: Error when getting information for file '/home/mhasoba/Documents/Teaching/IC_CMEE/2016-17/Assessment/StudentRepos/EmmaFox_EAF16/Week1/Code/.pdf': No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

** (evince:17409): WARNING **: Error setting file metadata: No such file or directory

======================================================================
Inspecting script file csvtospace.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: csvtospace.sh
# Desc: substitute the commas in the files with spaces
#		saves the output into a .txt file
# Arguments: 1-> comma-separated file 
# Date: 4 Oct 2016

echo "Creating a space delimited version of $1 ..."

cat $1 | tr -s "," " " >> $1.txt

echo "Done!"

exit
**********************************************************************

Testing csvtospace.sh...

Output (only first 500 characters): 

**********************************************************************
Creating a space delimited version of  ...
Done!

**********************************************************************

Code ran without errors

Time consumed = 0.01375s

======================================================================
Inspecting script file FirstExample.tex...

File contents are:
**********************************************************************
\documentclass [12 pt] {article}
\title{A Sample Document}
\author{Emma Fox}
\date{5 Oct 2016}
\begin{document}
	\maketitle
	
	\begin{abstract}
		This paper must be cool!
	\end{abstract}
	
	\section{Introduction}
		Blah! Blah!
	
	\section{Materials \& Methods}
	One of the most famous equations is:
	\begin{equation}
		E = mc^2
	\end{equation}
	This equation was first proposed by Einstein in 1905
	\cite{einstein1905does}.
	
	\bibliographystyle{plain}
	\bibliography{FirstBiblio}
\end{document}
**********************************************************************

Testing FirstExample.tex...

======================================================================
Inspecting script file MyExampleScript.sh...

File contents are:
**********************************************************************
#!/bin/bash

msg1="Hello"
msg2=$USER
echo "$msg1 $msg2"

echo "Hello $USER"
echo
**********************************************************************

Testing MyExampleScript.sh...

Output (only first 500 characters): 

**********************************************************************
Hello mhasoba
Hello mhasoba


**********************************************************************

Code ran without errors

Time consumed = 0.00401s

======================================================================
Inspecting script file FirstBiblio.bib...

File contents are:
**********************************************************************
@article{einstein1905does,
  title={Does the inertia of a body depend upon its energy-content},
  author={Einstein, Albert},
  journal={Annalen der Physik},
  volume={18},
  number={13},
  pages={639--641},
  year={1905}
}
**********************************************************************

Testing FirstBiblio.bib...

======================================================================
Inspecting script file UnixPrac1.txt...

File contents are:
**********************************************************************
#1 
wc -l {../Data/fasta/407228326.fasta,../Data/fasta/407228412.fasta,../Data/fasta/E.coli.fasta}

#2
tail -n +2 ../Data/fasta/E.coli.fasta

#3
$ tail -n +2 ../Data/fasta/E.coli.fasta | tr "\n" " "| wc -m

#4
tail -n +2 ../Data/fasta/E.coli.fasta | tr "\n" " " | grep -o ATGC | wc -l

#5
echo $(tail -n +2 ../Data/fasta/E.coli.fasta | tr "\n" " " | grep -o -e A -e T| wc -l) / $(tail -n +2 ../Data/fasta/E.coli.fasta | tr "\n" " " | grep -o -e G -e C| wc -l) | bc -l
**********************************************************************

Testing UnixPrac1.txt...

======================================================================
Inspecting script file tabtocsv.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: tabtocsv.sh
# Desc: substitute the tabs in the files with commas
#		saves the output into a .csv file
# Arguments: 1-> tab delimited file
# Date: 4 Oct 2016

echo "Creating a comma delimited version of $1 ..."

cat $1 | tr -s "\t" "," >> $1.csv

echo "Done!"

exit
**********************************************************************

Testing tabtocsv.sh...

Output (only first 500 characters): 

**********************************************************************
Creating a comma delimited version of  ...
Done!

**********************************************************************

Code ran without errors

Time consumed = 0.00483s

======================================================================
Inspecting script file boilerplate.sh...

File contents are:
**********************************************************************
#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: boilerplate.sh
# Desc: simple boilerplate for small shell scripts
# Arguments: none
# Date: 4 Oct 2016

echo -e "\nThis is a shell script! \n"

#exit
**********************************************************************

Testing boilerplate.sh...

Output (only first 500 characters): 

**********************************************************************

This is a shell script! 


**********************************************************************

Code ran without errors

Time consumed = 0.00355s

======================================================================
======================================================================
Finished running scripts

Ran into 4 errors

======================================================================
======================================================================

FINISHED WEEKLY ASSESSMENT

Current Marks for the Week = 97.5

NOTE THAT THESE ARE NOT THE FINAL MARKS FOR THE WEEK, BUT UPPER BOUND ON THE MARKS!