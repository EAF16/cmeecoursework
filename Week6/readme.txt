readme for Week6 Directory

├── Code
│   └── Population_Genomics_Practical.R - This script runs through data on human chromosome 15 to look for adaptive differences between populations. Part of practical 2.
├── Data
│   ├── H938_chr15.geno - Data for Population_Genomics_Practical.R
│   ├── primate_aligned.fasta - Data collected during practical 1
│   └── primate_raw.fasta - Data collected during practical 1
├── Reports
│   ├── Handout01.txt - Answers to questions from practical 1
│   ├── Handout02.txt - Answers to questions from practical 2
│   └── Handout03.txt - Answers to questions from practical 3
├── Results
│   ├── Population_Genomics_Plots
│   │   ├── 01_Summary_of_Count_Distribution.pdf - shows the histogram for the number of observations.
│   │   ├── 02_Frequency_of_Major_vs_Minor.pdf - Shows the frequency of the major allele vs the minor allele.
│   │   ├── 03_Genotype_vs_Allele_Frquencies.pdf - Shows the frequency of the 3 genotypes in relation to the major allele frequency with ideal Hardy-Weinberg lines plotted. 
│   │   ├── 04_Checking_Pval_Distribution.pdf - Looks to see if the pvalues calculated for each locus are normally distributed.
│   │   ├── 05_Observed_vs_Expected_Heterozygosity.pdf - Observed vs expected heterozygosity at each locus.
│   │   ├── 06_Loci_Departing_from_HWEquilibrium.pdf - Shows the SNP number and how much the genotype frequencies depart from Hardy Weinberg equilibrium.
│   │   └── 07_Departure_Using_SlidingWindows.pdf - Uses a sliding windows approach to find areas of the chromosome that show a spike in departure from Hardy Weinberg equilibrium. 
│   └── Spatial_Methods_Plots
│       ├── Dataset1_CVerror_plot.pdf - Part of practical 3. Error plot from season 1. Used for finding the best value of K.
│       ├── Dataset1_MDS_plot.pdf - Part of practical 3. Shows clusters of similar allele frequencies of individuals from survey season 1. 
│       ├── Dataset1_plot.pdf - Part of practical 3. A plot showing the genome similarity between populations in survery season 1.
│       ├── Dataset2_CVerror_plot.pdf - Part of practical 3. Error plot from season 2. Used for finding the best value of K.
│       ├── Dataset2_MDS_plot.pdf - Part of practical 3. Shows clusters of similar allele frequencies of individuals from survey season 2.
│       ├── Dataset2_plot.pdf - Part of practical 3. A plot showing the genome similarity between populations in survery season 2.
│       ├── Dataset3_CVerror_plot.pdf - Part of practical 3. Error plot from season 3. Used for finding the best value of K.
│       ├── Dataset3_MDS_plot.pdf - Part of practical 3. Shows clusters of similar allele frequencies of individuals from survey season 3.
│       └── Dataset3_plot.pdf - Part of practical 3. A plot showing the genome similarity between populations in survery season 3.
└── Sandbox
    └── ME_Sample_Localities.gif.pdf.gz - Map of Middle Earth for intepreting the results of practical 3
