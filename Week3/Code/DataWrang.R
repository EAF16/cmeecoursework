#FileName: DataWrang.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This script is used to practice working with a real data set (the Pound Hill data) including importing, 
#formatting, and visualizing. 

##ARGUMENTS:
#Pound hill data set

##OUTPUT:
#Faceted graph and full formatted data frame.

################################################################
################## Wrangling the Pound Hill Dataset ############
################################################################

############# Load the dataset ###############
# header = false because the raw data don't have real headers
#Open as a matrix to prevent R classifying the data as factors
MyData <- as.matrix(read.csv("../Data/PoundHillData.csv",header = F)) 

# header = true because we do have metadata headers
MyMetaData <- read.csv("../Data/PoundHillMetaData.csv",header = T, sep=";", stringsAsFactors = F)
#don't want R to convert everything to factors

############# Inspect the dataset ###############
head(MyData) #see the first few rows with their headers
dim(MyData) #lists the dimensions. 45 rows and 60 columns
str(MyData) #lists the classification of the data columns

############# Transpose ###############
MyData <- t(MyData) #flipped the rows and columns
head(MyData) #Now the row represents a single observation rather than the columns. all the columns are the same type
            #The headers are now categories rather than measures
dim(MyData)

############# Replace species absences with zeros ###############
MyData[MyData == ""] = 0 #empty spots in the matrix are now equal to 0

############# Convert raw matrix to data frame ###############

TempData <- as.data.frame(MyData[-1,],stringsAsFactors = F) #stringsAsFactors = F is important!
#deleted first now because it's the names of the columns and then assigns it the way you want in the next one
colnames(TempData) <- MyData[1,] # assign column names from original data

############# Convert from wide to long format  ###############
require(reshape2) # load the reshape2 package

?melt #check out the melt function
#melt specifies whether columns are grouping factors or measures
MyWrangledData <- melt(TempData, id=c("Cultivation", "Block", "Plot", "Quadrat"), 
variable.name = "Species", value.name = "Count")
#sets the data type of each column
MyWrangledData[, "Cultivation"] <- as.factor(MyWrangledData[, "Cultivation"])
MyWrangledData[, "Block"] <- as.factor(MyWrangledData[, "Block"])
MyWrangledData[, "Plot"] <- as.factor(MyWrangledData[, "Plot"])
MyWrangledData[, "Quadrat"] <- as.factor(MyWrangledData[, "Quadrat"])
MyWrangledData[, "Count"] <- as.numeric(MyWrangledData[, "Count"])

str(MyWrangledData)
head(MyWrangledData)
dim(MyWrangledData)

############# Start exploring the data (extend the script below)!  ###############
#This function generates a lattic graph that shows the different counts
#in response to each treatment by species
require(lattice)
print(bwplot(log(Count) ~ Block | factor(Species), data = MyWrangledData))