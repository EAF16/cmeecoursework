#FileName: PP_Regress.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This function creates a csv file containing the summary stastics of all the regression equations
#for prey mass/predator mass divided into subsets by type of feeding interaction, predator lifestage,
#and location. Extra credit for practical 3.

##ARGUMENTS:
#EcolArchives data frame used in previous exercises

##OUTPUT:
#PDF of the graph
#and a .csv file containing the summary stastics of all the regression equations.

library(ggplot2)
library(plyr)
library(dplyr)

#Imports the file we're working on
MyDF <- read.csv("../Data/EcolArchives-E089-51-D1.csv")

#Creates a function that runs a linear model in a data set with Predator.mass as the response and Prey.mass as the explanatory
#variable. It then extracts slope, intercept, r squared value, p value, and f statistic and puts them in a data frame.
LinMod <- function(x) {
  MyLM <- lm(Predator.mass~Prey.mass, data = x)
  Regression.slope <- coef(summary(MyLM))[2]
  Regression.intercept <- coef(summary(MyLM))[1]
  R.squared <- summary(MyLM)$r.squared
  P.value <- summary(MyLM)$coefficients[7]
  F.statistic <- summary(MyLM)$fstatistic[1]
  #Tells R to ignore null F statistic results and keep going
  if(!is.null(F.statistic)){
    #compiles results into a table 
    Results <- data.frame(Regression.slope, Regression.intercept, R.squared,
                          F.statistic, P.value)
    return(Results)
  }
}

#ddply will compile a data frame of the lm coefficients for the models in the specified subsets
Results.Frame<- ddply(MyDF, .(Type.of.feeding.interaction, Predator.lifestage, Location), LinMod)

#Puts the data frame into a csv file
write.csv(Results.Frame, "../Results/PP_Regress_loc_Results.csv", row.names = FALSE)
