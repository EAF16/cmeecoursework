#FileName: apply2.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This function is to practice using the apply function in conjunction with a function we defined

##ARGUMENTS:
# Matrix created

##OUTPUT:
# The matrix with each term over 0 multiplied by 100 

#if any term v is greater than 0, multiply it by 100 and return it
SomeOperation <- function(v){
  if (sum(v) > 0){
    return(v * 100)
  }
  return(v)
}

#created a 10x10 matrix
M <- matrix(rnorm(100), 10, 10)

#apply the function we just defined to each row
print(apply(M, 1, SomeOperation))