#FileName: Vectorize2.R
#Author: 

#This function models the population size of 1000 different populations of random starting
#size for 100 years incorporating gaussian fluctuations.

##ARGUMENTS:
#1000 x 100 matrix with the first row filled in with random values.

##OUTPUT:
#1000 column matrix showing 100 years of population size fluctuation. 

# Runs the stochastic (with gaussian fluctuations) Ricker Eqn .

rm(list=ls())

#p0 is a random population of size 1000 with min of .5 and max of 1.5
#r is the growth rate, k is the carrying capacity, 
#will run for 100 years
stochrick<-function(p0=runif(1000,.5,1.5),r=1.2,K=1,sigma=0.2,numyears=100)
{
  #initialize
  #creates empty matrix with 100 rows and 1000 columns representing different starting points
  N<-matrix(NA,numyears,length(p0))
  #Row 1 is the population starting point
  N[1,]<-p0
  
  #For each new hypothetical population
  for (pop in 1:length(p0)) #loop through the populations
  {
    #Run through the following function for year 2 through the number of years specified
    for (yr in 2:numyears) #for each pop, loop through the years
    {
      #The populations new value in the year specified is equal to 
      #The populations size the year before times e raised to: 1 minus the population in the previous year divided by K
      #plus a randomly generated number to represent random error (standard deviation of 0.2)
      N[yr,pop]<-N[yr-1,pop]*exp(r*(1-N[yr-1,pop]/K)+rnorm(1,0,sigma))
    }
  }
  return(N)
}
# Now write another code called stochrickvect that vectorizes the above 
# to the extent possible, with improved performance: 

# print("Vectorized Stochastic Ricker takes:")
# print(system.time(res2<-stochrick()))

