#FileName: Vectorize1.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#This function compares the running speed of summing a matrix between a for loop and the inbuilt sum function

##ARGUMENTS:
# Matrix created

##OUTPUT:
# Time it takes to run the two function

#Creates a 1000x1000 matrix of random numbers between 0 and 1
#Then tells the function for every i in the rows and every j in the columns, add them both to the total
M <- matrix(runif(1000000), 1000, 1000)
SumAllElements <- function(M){
  Dimensions <- dim(M)
  Tot <- 0
  for (i in 1:Dimensions[1]){
    for (j in 1:Dimensions[2]){
      Tot <- Tot + M[i,j]
    }
  }
  return(Tot)
}

#These commands will tell us how long it took to sum the matrix with each method
##This takea about 1 sec compared to 
print(system.time(SumAllElements(M)))

##this which takes about 0.01 sec
print(system.time(sum(M)))

#sum is faster because it uses vectorization to avoid the looping above