#FileName: basic_io.R
#Author: "Emma Fox (e.fox16@imperial.ac.uk)"

#A simple R script to illustrate R input-output.
#Run line by line and check inputs outputs to understand what is
#happening

##ARGUMENTS:
# tree.csv from data

##OUTPUT:
# MyData.csv file with row names

#imports trees file with headers
MyData <- read.csv("../Data/trees.csv", header = TRUE)

#write it out as a new file
write.csv(MyData, "../Results/MyData.csv")

#append to it. disregard warning message.
#doing write.table because you will not always have csv files
#do have to specify the seperator for write table though
write.table(MyData[1,], file = "../Results/MyData.csv", append = TRUE)

#write row names
write.csv(MyData, "../Results/MyData.csv", row.names = TRUE)

#ignore column names
write.table(MyData, "../Results/MyData.csv", col.names = FALSE)
