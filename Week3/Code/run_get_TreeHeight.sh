#!/bin/bash
# Author: Emma Fox e.fox16@imperial.ac.uk
# Script: run_get_treeheights.sh
# Desc: Tests to see if get_treeheights runs and generates the correct file
# Arguments: runs an R script
# Date: 19 Oct 2016

#Run script
echo -e "\nRunning get_TreeHeight.R"
Rscript get_TreeHeight.R ../Data/trees.csv

#Tell the user the script is checking to see if the right file was created
echo -e "\nChecking to see if the script created ../Results/trees_TreeHts.csv \n"

#If the files is not found, return the first set of strings
#If the file is found, print the second set of strings
if [ ! -f ../Results/trees_treeheights.csv ]
	then
		echo -e "\nFile HAS NOT been created"
		echo -e "R script has FAILED\n"
	else 
		echo -e "\nFile HAS been created"
		echo -e "R Script was run CORRECTLY\n"
fi

#exit

