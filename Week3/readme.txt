readme for Week3 Directory

── Code
│   ├── apply1.R - This function is to practice using the apply function 
					to take the mean and variance of rows and columns.
					
│   ├── apply2.R - This function is to practice using the apply function 
					in conjunction with a function we defined.
					
│   ├── basic_io.R - A simple R script to illustrate R input-output 
					(reading and writing files from a script).
					 
│   ├── boilerplate.R - This function prints the class of the two 
						arguments input.
						
│   ├── break.R - This function is to practice using the break command 
					to stop the running of a loop.
					
│   ├── browse.R - This script is used to test out the browse function 
					which allows step by step exploration within the 
					code as it runs. This specific example is on 
					exponential growth.
					
│   ├── control.R - This script is used to practice with control 
					statements including loops and if statements.
					
│   ├── DataWrang.R - This script is used to practice working with a 
					real data set (the Pound Hill data) including 
					importing, formatting, and visualizing. 
					
│   ├── get_TreeHeight.R - This function calculates heights of trees
							supplied in the file specified in the 
							command line from the angle of elevation and 
							the distance from the base using the 
							trigonometric formula 
							height = distance * tan(radians). Part of 
							practical 3.
|   ├── GPDD_mapping. R - This script practices working with GIS data 
						and the maps package. It overlays points from an 
						imported file onto a world map

│   ├── next.R - This function is to practice using the next command to 
				get a control flow to move on to the next i.
				
│   ├── PP_Lattice.R - This function generates a desnity plot for 
						predator mass, prey mass, and prey mass/predator 
						mass and a csv file containing the mean and 
						median of each category.
						
│   ├── PP_Regress_loc.R - This function creates a csv file containing 
						the summary stastics of all the regression 
						equations for prey mass/predator mass divided 
						into subsets by type of feeding interaction, 
						predator lifestage, and location. Extra credit 
						for practical 3.
						
│   ├── PP_Regress.R - This function creates a pdf with faceted graph 
						showing the relation between predator mass and 
						prey mass on a logarithmic scale and subdivided 
						into type of feeding interaction and then 
						plotted with a linear regression according to 
						predator lifestage. It also returns a csv file 
						containing the slope, intercept, r squared, p 
						value, and f statistic for each regression shown 
						on the graph. Part of practical 3.
						
│   ├── run_get_TreeHeight.sh - Tests to see if get_treeheights runs and 
						generates the correct file. Part of practical 3.
						
│   ├── sample.R - This function is to practice generating random 
					numbers, taking samples, and calculating the mean
					using both vectorization and for loops.
					
│   ├── stochrickvect.R - The vectorized form of the stochastic Ricker 
							equation found in Vectorize2.R. Part of 
							practical 3. 
							
│   ├── TAutoCorr.R - This script tests the correlation between historic 
						annual mean temperatures in Key West. It 
						calculates the overall correlation coefficient 
						and then uses 10000 random samplings of the data 
						and calculates the percentage that are over the 
						intial correlation coefficient. Part of 
						practical 3. 
						
│   ├── TAutoCorr.tex - This is the source code for the LaTeX file
						explaining TAutoCorr.R
	
│   ├── TreeHeight.R - This function calculates heights of trees from 
						the angle of elevation and the distance from the 
						base using the trigonometric formula
						height = distance * tan(radians).
						
│   ├── try.R - This function is to practice the try command on the 
				script we already used to practice generating random 
				numbers, taking samples, and calculating the mean
				using both vectorization and for loops.
				
│   ├── Vectorize1.R - This function compares the running speed of 
						summing a matrix between a for loop and the 
						inbuilt sum function.
						
│   └── Vectorize2.R - Copied from silbiomasterepo. Contains the 
						stochastic Ricker equation with guassian 
						fluctuations. Part of practical 3 required this 
						script be vectorized.
						
						
├── Data
│   ├── EcolArchives-E089-51-D1.csv - Data frame containing feeding 
								interaction data between predator and
								prey.
					
│   ├── GPDDFiltered.RData - Species map points for the mapping extra 
							credit in practical 3. 
								
│   ├── KeyWestAnnualMeanTemperature.RData - Mean annual temperatures in 
									Key West.
									
│   ├── PoundHillData.csv - Data on species abundance by plot for 
							different treatments. 
		
│   ├── PoundHillMetaData.csv - Raw data from Pound Hill observations.

│   ├── Results.txt - Data for the MyBars plot

│   └── trees.csv - Tree measurements of species, distance, and angle to
					calculate tree height. 
├
└── Results
    ├── Girko.pdf - Plot of eigenvalues exemplifying Girko's circular 
					law and how to plot two data frames on one graph.
					
	├── GPDD_Map.pdf - Plot of eigenvalues exemplifying Girko's circular 
					law and how to plot two data frames on one graph.
					
    ├── MyBars.pdf - Plot of Results.txt. Used as practice for working
					with lineranges and annotations. 
					
    ├── MyData.csv - Created by basic_io.R. Used to practice reading and
					writing csv files. 
					
    ├── MyFirst-ggplot2-Figure.pdf - Practicing with ggplot2. Plotting
								log predator mass vs log prey mass.
								 
    ├── MyLinReg.pdf - Plotting an ideal linear regression fro randomly
						generated data.
						
    ├── PP_Regress_loc_Results.csv - linear model outputs for EcolArchives
					data subsetted by feeding interaction, lifestage, and
					location. Extra credit for practical 3.
						
    ├── PP_Regress.pdf - Faceted graph of predator vs prey mass by type 
						of feeding interaction and predator lifestage.
						Part of PP_Regress for practical 3. 
						
    ├── PP_Regress_Results.csv - linear model outputs for EcolArchives
					data subsetted by feeding interaction and lifestage
					for practical 3.
						
    ├── PP_Results.csv - Mean and median of prey mass, predator mass, 
						and prey mass/predator mass. Part of PP_Lattice
						for practical 3. 
						
    ├── Predator_Lattice.pdf - Density plot from PP_Lattice for predator
								mass by type of feeding interaction.
								
    ├── Pred_Prey_Overlay.pdf - Overlayed histogram of predator and prey
								masses. 
    
    ├── Prey_Lattice.pdf - Density plot from PP_Lattice for prey mass by 
							type of feeding interaction.
							
    ├── TAutoCorr_Plot.pdf - Graph for LaTeX output. Plots offset 
							temperature sets by each other.

    ├── TAutoCorr.pdf - LaTeX output for interpretation of TAutoCorr.pdf

    ├── SizeRatio_Lattice.pdf - Density plot from PP_Lattice for prey 
								mass divided by predator mass and 
								faceted by type of feeding interaction.
																
    ├── TreeHts.csv - Created by TreeHeight.R. A table of the heights of
					the trees surveyed calculated using trigonometric
					principles.
    
    └── trees_treeheights.csv - Created by get_TreeHeight.R. A table of 
								the heights of the trees surveyed 
								calculated using trigonometric 
								principles. Any file may be input and 
								the output will be named using the same
								filename followed by _treeheights.csv
    
